export default {
  path     : '/Index/Nav3',
  component: require("components/Index/Home/Home"),
  name     : '',
  iconCls  : 'fa fa-address-card',
  leaf     : true, //只有一个节点
  children : [
    require('router/Index/Nav3/Page6').default,
  ]
}