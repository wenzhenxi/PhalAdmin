export default {
  path     : '/Index/Charts',
  component: require("components/Index/Home/Home"),
  name     : 'Charts',
  iconCls  : 'fa fa-bar-chart',
  children : [
    require('router/Index/Charts/Echarts').default,
  ]
}