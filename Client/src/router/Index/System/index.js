export default {
  path: '/Index/System',
  component: require("components/Index/Home/Home"),
  name: '系统管理',
  iconCls: 'fa fa-gears fa-lg', //图标样式class
  children: [
    require('router/Index/System/User').default,
    require('router/Index/System/Authority').default,
    require('router/Index/System/Role').default,
    require('router/Index/System/Project').default,
    require('router/Index/System/Api').default,
    require('router/Index/System/Menu').default,
  ]
}

