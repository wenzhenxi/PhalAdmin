export default {
  path: '/Index',
  component: require("components/Index/Index"),
  // 菜单Log跳转地址
  logopath : '/Index/System/Welcome',
  children: [
    require("router/Index/System").default,
    require("router/Index/Welcome").default,
    require("router/Index/Nav1").default,
    require("router/Index/Nav2").default,
    require("router/Index/Nav3").default,
    require("router/Index/Charts").default
  ]
}

