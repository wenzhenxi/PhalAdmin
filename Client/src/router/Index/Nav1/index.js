export default {
  path: '/Index/Nav1',
  component: require("components/Index/Home/Home"),
  name: '导航一',
  iconCls: 'el-icon-message', //图标样式class
  children: [
    require('router/Index/Nav1/Form').default,
    require('router/Index/Nav1/Page3').default,
    require('router/Index/Nav1/Table').default
  ]
}

