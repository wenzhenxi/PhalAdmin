export default {
  path     : '/Index/Nav2',
  component: require("components/Index/Home/Home"),
  name     : '导航二',
  iconCls  : 'fa fa-id-card-o',
  children : [
    require('router/Index/Nav2/Page4').default,
    require('router/Index/Nav2/Page5').default,
  ]
}