export default {
  path     : '/login',
  component: require("components/Login/Login.vue"),
  hidden   : true
}