let routes = [
   // 登录路由
   require("router/Login").default,
   // 系统主路由(必须放到第二位才能被读取生成菜单)
   require("router/Index").default,
   // 默认路由访问/目录跳转地址
   {
      path: '/',
      hidden: true,
      redirect: { path: '/login' }
   },
   // 404页面路由
   {
      path: '/404',
      component: require('components/404/404'),
      name: '',
      hidden: true
   },
   // 当访一个不存在的路由会在此处理
   {
      path: '*',
      hidden: true,
      redirect: { path: '/404' }
   }
];



export default routes;