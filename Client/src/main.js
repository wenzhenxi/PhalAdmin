// 游览器适配
import babelpolyfill from 'babel-polyfill'
// 初始化Vue以及入口
import Vue from 'vue'
import App from './App'
// 加载elementUI库
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-default/index.css'
// 加载路由机制
import VueRouter from 'vue-router'
import routes from './router/router'
// 加载vuex
import store from './vuex/store'
// 加载页面顶部进度条
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
// 加载Session
import Session from 'lib/Session'
// 引入CSS文件
import 'assets/theme/theme-green/index.css'
import 'font-awesome/css/font-awesome.min.css'

Vue.use(ElementUI);
Vue.use(VueRouter);

// 初始化路由
const router = new VueRouter({
    routes,
    // 开启history模式
    mode: 'history',
    base: __dirname
});

// 设置加载条效果
NProgress.configure({ showSpinner: false });

// 路由开始触发
router.beforeEach((to, from, next) => {
    NProgress.start();
    // 对用户登录状态进行处理
    if (to.path == '/login') {
        Session.del("LoginInfo");
    }
    let user = Session.get("LoginInfo");
    if (!user && to.path != '/login') {
        next({ path: '/login' });
    } else {
        next();
    }
});

// 路由结束触发
router.afterEach(transition => {
    NProgress.done();
});


new Vue({
    el: '#app',
    template: '<App/>',
    router,
    store,
    components: { App }
    //render: h => h(Login)
}).$mount('#app');

