// 加载接口请求工具axios
import axios from 'axios'

import SERVICE from 'config/service.json'
console.log(SERVICE)
export default (objectname, api, data) => {
    var responseData = {}

    // 如果是debug模式 打印请求数据
    if (SERVICE["debug"]) {
        console.log(data)
    }

    if (SERVICE["Server"][objectname] == null) {
        alert("服务不存在!!!");
        throw "服务不存在!!!";
    }

    var ax = axios.get(SERVICE["Server"][objectname] + api, { params: data })
        .catch(function (error) {
            alert("服务器抛锚!!!");
            throw error;
        }).then(function (response) {
            if (SERVICE["debug"]) {
                // 如果是debug模式 打印返回结果
                console.log(response.data)
            }
            return response.data
        });

    return ax
}

