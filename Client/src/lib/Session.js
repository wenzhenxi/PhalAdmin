class Session {
    constructor() {

    }
    set(key, value) {
        sessionStorage[key] = encodeURI(JSON.stringify(value))
    }
    get(key) {
        if (sessionStorage.hasOwnProperty(key)) {
            return JSON.parse(decodeURI(sessionStorage[key]))
        } else {
            return false
        }
    }
    del(key) {
        sessionStorage[key] = null
    }
}
export default new Session

