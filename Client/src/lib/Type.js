/**
 * mutation-types
 * Created by yidie on 2017/5/26.
 */

  //module Employee
  // get state

  // Login模块
export const LOGIN_GET_LOGINING = 'login/getLoginIng';
export const LOGIN = 'login/LOGIN';
export const LOGIN_LOGINING_START = 'login/LOGINING_START';
export const LOGIN_LOGINING_STOP = 'login/LOGINING_STOP';

  // User模块
export const USER_UPDATE_ROLE = 'user/UPDATE_ROLE';
export const USER_UPDATE_TYPE = 'user/UPDATE_TYPE';
export const USER_DEL = 'user/DEL';
export const USER_ADD = 'user/ADD';
export const USER_UPDATE = 'user/UPDATE';
export const USER_LIST = 'user/LIST';

  // Role模块
export const ROLE_LIST = 'role/LIST';
export const ROLE_SET_LIST = 'role/INIT_LIST';
export const ROLE_GET_LIST = 'role/GET_LIST';
