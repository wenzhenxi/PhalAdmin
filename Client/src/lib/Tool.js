 import md5 from 'md5'

 class Tool {
     // 随机数生成
     randomWord(randomFlag, min, max) {
             // randomWord 产生任意长度随机字母数字组合
             // randomFlag-是否任意长度 min-任意长度最小位[固定位数] max-任意长度最大位
             var arry = [];
             var str = "",
                 range = min,
                 arr = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
             arry = '0123456789';

             // 随机产生
             if (randomFlag) {
                 range = Math.round(Math.random() * (max - min)) + min;
             }

             var pos;
             var len = arr.length - 1;
             var len1 = arry.length - 1;
             for (var i = 0; i < range; i++) {

                 if (i > 2) {
                     pos = Math.round(Math.random() * len1);
                     str += arry[pos];
                 } else {
                     pos = Math.round(Math.random() * len);
                     str += arr[pos];
                 }
             }
             return str;
         }
         // 密码加密规则
     passEnCode(pass) {
         return md5(pass)
     }

 }
 export default new Tool

