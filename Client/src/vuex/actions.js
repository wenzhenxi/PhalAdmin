export default {

    'todos/increment': ({ commit } , i) => {

        commit('todos/INCREMENT', i)
    },
    'todos/decrement': ({ commit } , i) => {
        commit('todos/DECREMENT', i)
    }
}

