import Vue from 'vue'
import Vuex from 'vuex'
// import actions from './actions'
// import getters from './getters'

Vue.use(Vuex)

// // 应用初始状态
// const state = {
//      a :{
//        count: 10
//      }
//
// }
//
// const INCREMENT = "todos/INCREMENT";
// const DECREMENT = "todos/DECREMENT";
//
// // 定义所需的 mutations
// const mutations = {
//     "todos/increment":function(state,{ i }) {
//       console.log(i)
//         state.a.count += i
//     },
//     "todos/decrement":function(state,{ i }) {
//       console.log(i)
//         state.a.count -= i
//     }
// }
//
//
//
// const moduleA = {
//       state: state,
//       mutations: mutations,
//       actions: actions,
//       getters: getters
// }


// 创建 store 实例
export default new Vuex.Store({
    // actions,
    // getters,
    // state,
    // mutations,

    modules: {
        a:require("components/Index/Nav2/Page4/Page4Action.js").default,
        login:require("components/Login/LoginAction.js").default,
        user:require("components/Index/System/User/UserAction.js").default,
        role:require("components/Index/System/Role/RoleAction2.js").default,
    }
})