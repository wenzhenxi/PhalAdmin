import { Api, Session } from "lib"

export const getRoleList = (keyword) => {

  var rs = Session.get("LoginInfo")
  return Api("manage", "Role.getRoleList", { uId: rs.uId, keyword: keyword })
}

export const getSimplePrivilegeList = () => {

    var rs = Session.get("LoginInfo")
    return Api("manage", "Privilege.getSimplePrivilegeList", { uId: rs.uId })
  }
  //创建角色
export const setRole = (name, info, privilegeList) => {
    var rs = Session.get("LoginInfo")
    return Api("manage", "Role.setRole", { uId: rs.uId, name: name, info: info, privilegeList: privilegeList })
  }
  //更新角色
export const updateRole = (rId, name, info, privilegeList) => {
    var rs = Session.get("LoginInfo")
    return Api("manage", "Role.updateRole", { uId: rs.uId, rId: rId, name: name, info: info, privilegeList: privilegeList })
  }
  //更改启用状态
export const editStatus = (rId, type) => {
    var rs = Session.get("LoginInfo")
    return Api("manage", "Role.updateRoleType", { rId: rId, uId: rs.uId, type: type })
  }
  //删除角色
export const delRole = (rId) => {
  var rs = Session.get("LoginInfo")
  return Api("manage", "Role.deleteRole", { rId: rId, uId: rs.uId })
}

