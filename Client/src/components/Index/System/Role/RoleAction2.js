// 加载基础库
import { Api, Session, Tool, Type } from "lib"

// 应用初始参数
const state = {
  role: {
    RolesData: []
  }
};

// 定义参数获取方式
const getters = {
  [Type.ROLE_GET_LIST](state) {
    return state.role.RolesData
  }
};

// 定义所需的 mutations
const mutations = {
  [Type.ROLE_SET_LIST](state, { data }) {
    state.role.RolesData = data
  }
};

const actions = {
  [Type.ROLE_LIST](context, _this) {
    var LoginInfo = Session.get("LoginInfo")
    var rs = Api("manage", "Role.getRoleList", { uId: LoginInfo.uId, keyword: '' })
    rs.then(function (response) {
      if (response.ret == 200) {
        context.commit(Type.ROLE_SET_LIST, { data: response.data });
      } else {
        _this.$notify({ title: '异常', message: response.msg, type: 'error', });
      }
    })
  }
};

export default {
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
}

