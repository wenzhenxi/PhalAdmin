var phoneRules = (rule, value, callback) => {
  if (value == "") {
    callback("请输入联系电话")
  }
  if (!(/^1[34578]\d{9}$/.test(value))) {
    callback(new Error('手机号码有误，请重填'));
  }
  return callback()
};

export const editRoleFormRules = {
    roles: [{
        required: true,
        message: '请选择角色',
        trigger: 'change',
        type: 'array'
    }]
}

export const editFormRules = {
  name: [{
    required: true,
    message: '请输入联系人姓名',
    trigger: 'blur'
  }, {
    min: 2,
    max: 32,
    message: '联系人姓名2~32位',
    trigger: 'blur'
  }],
  userName: [{
    required: true,
    message: '请输入登录用户名',
    trigger: 'blur'
  }, {
    min: 5,
    max: 32,
    message: '登录用户名5~32位',
    trigger: 'blur'
  }],
  phone: [{
    required: true,
    validator: phoneRules,
    trigger: 'blur'
  }],
  email: [{
    required: true,
    message: '请输入邮箱地址',
    trigger: 'blur'
  }, {
    type: 'email',
    message: '请输入正确的邮箱地址',
    trigger: 'blur'
  }]
}

