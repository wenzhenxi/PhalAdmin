// 加载基础库
import { Api, Session, Tool, Type } from "lib"
import moment from 'moment'

// 应用初始参数
const state = {
  role: {
    btnEditText: "提 交",       // 提交按钮名称
    editLoading: false,        // 编辑确认动画状态
    listLoading: false,        // 列表加载动画状态
    editFormVisible:false,     // 用户编辑界面是否可见
    editRoleFormVisible: false, // 角色编辑界面是否可见
    userNameDisabled: false,   // 用户名称是否可以修改
    editFormTitle: "编辑"       // 编辑界面名称
  }
};

// 定义参数获取方式
const getters = {
  [Type.ROLE_GET_btnEditText](state) {
    return state.role.btnEditText
  }
};

// 定义所需的 mutations
const mutations = {
  [Type.LOGIN_SET_btnEditText](state,value){
    state.login.logining = value
  }
};

const actions = {

  // 获取用户列表
  [Type.USER_LIST](context, _this) {
    _this.listLoading = true;
    var rs = Api("manage", "Manage.getManageList", { uId: Session.get("LoginInfo").uId, keyword: _this.formInline.keyword })
    rs.then(function (response) {
      if (response.ret == 200) {
        _this.tableData = response.data
      } else {
        _this.$notify({ title: '异常', message: response.msg, type: 'error' });
      }
    })
    _this.listLoading = false;
  },

  // 更新用户信息
  [Type.USER_UPDATE](context, _this) {
    var rs = Api("manage", "Manage.updateManageInfo", {
      uId: Session.get("LoginInfo").uId,
      ouId: _this.editForm.uId,
      name: _this.editForm.name,
      phone: _this.editForm.phone,
      email: _this.editForm.email
    })
    rs.then(function (response) {
      _this.editLoading = false;
      _this.btnEditText = '提 交';
      if (response.ret == 200) {
        //编辑
        for (var i = 0; i < _this.tableData.length; i++) {
          if (_this.tableData[i].uId == _this.editForm.uId) {
            _this.tableData[i].name = _this.editForm.name;
            _this.tableData[i].userName = _this.editForm.userName;
            _this.tableData[i].phone = _this.editForm.phone;
            _this.tableData[i].email = _this.editForm.email;
            break;
          }
        }
        _this.$notify({ title: '成功', message: '修改成功', type: 'success' });
        _this.editFormVisible = false;
      } else {
        _this.$message.error(response.msg);
      }
      _this.listLoading = false;
    })
  },

  // 新增用户
  [Type.USER_ADD](context, _this) {
    var password = 'sunmi388'
      //var password = Tool.randomWord(false, 8, 8)
    var rs = Api("manage", "Manage.createManage", {
      uId: Session.get("LoginInfo").uId,
      userName: _this.editForm.userName,
      name: _this.editForm.name,
      phone: _this.editForm.phone,
      email: _this.editForm.email,
      passWord: Tool.passEnCode(password)
    })

    console.log(password)

    rs.then(function (response) {
      _this.editLoading = false;
      _this.btnEditText = '提 交';
      if (response.ret == 200) {
        _this.tableData.push({
          uId: response.data,
          name: _this.editForm.name,
          userName: _this.editForm.userName,
          phone: _this.editForm.phone,
          email: _this.editForm.email,
          CreateTimeThe: moment().format('YYYY-MM-DD HH:mm:ss'),
          addr: _this.editForm.addr
        });
        _this.$notify({ title: '成功', message: '提交成功', type: 'success' });
        _this.editFormVisible = false;
      } else {
        _this.$message.error(response.msg);
      }
      _this.listLoading = false;
    })
  },

  // 删除用户
  [Type.USER_DEL](context, { _this, ouId }) {
    var rs = Api("manage", "Manage.deleteManage", { uId: Session.get("LoginInfo").uId, ouId: ouId })
    rs.then(function (response) {
      if (response.ret == 200) {
        for (var i = 0; i < _this.tableData.length; i++) {
          if (_this.tableData[i].uId == ouId) {
            _this.tableData.splice(i, 1);
            break;
          }
        }
        _this.$notify({ title: '成功', message: '删除成功', type: 'success' });
      } else {
        _this.$notify({ title: '异常', message: response.msg, type: 'error', });
      }
    })
  },

  // 修改用户的停用启用状态
  [Type.USER_UPDATE_TYPE](context, { _this, type, ouId }) {
    var rs = Api("manage", "Manage.updateManageType", { uId: Session.get("LoginInfo").uId, type: type ? 1 : 0, ouId: ouId })
    rs.then(function (response) {
      if (response.ret == 200) {
        var typeDescribe = type == true ? "启用成功" : "停用成功";
        _this.$notify({ title: '成功', message: typeDescribe, type: 'success', duration: 1000 });
      } else {
        _this.$notify({ title: '异常', message: response.msg, type: 'error', });
        type = !type
      }
    })
  },

  // 修改用户对应角色清单
  [Type.USER_UPDATE_ROLE](context, _this) {
    var params = _this.editForm
    var roles = JSON.stringify(params.roles)
    var rs = Api("manage", "Manage.updateRoles", { uId: Session.get("LoginInfo").uId, ouId: params.uId, roles: roles })
    _this.editLoading = true;
    _this.btnEditText = '提交中';
    // 编辑
    rs.then(function (response) {
      _this.editLoading = false;
      _this.btnEditText = '提 交';
      if (response.ret == 200) {
        for (var i = 0; i < _this.tableData.length; i++) {
          if(_this.tableData[i].uId == params.uId){
            _this.tableData[i].role = params.roles;
            break;
          }
        }
        //编辑
        _this.$notify({ title: '成功', message: '分配角色成功', type: 'success' });
        _this.editRoleFormVisible = false;
      } else {
        _this.$message.error(response.msg);
      }
    })
  }
};

export default {
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
}

