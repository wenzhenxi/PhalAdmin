import { Api, Session } from "lib"

export const getProjectList = () => {

        var rs = Session.get("LoginInfo")
        return Api("manage", "Project.getProjectList", { uId: rs.uId })
    }
    //创建角色
export const addProject = (name) => {
        var rs = Session.get("LoginInfo")
        return Api("manage", "Project.addProject", { uId: rs.uId, name: name })
    }
    //更新角色
export const updateProject = (pId, name) => {
        var rs = Session.get("LoginInfo")
        return Api("manage", "Project.updateProject", { uId: rs.uId, pId: pId, name: name })
    }
    //更改启用状态
export const editStatus = (pId, type) => {
        var rs = Session.get("LoginInfo")
        return Api("manage", "Project.updateProjectType", { pId: pId, uId: rs.uId, type: type })
    }
    //删除角色
export const delProject = (pId) => {
    var rs = Session.get("LoginInfo")
    return Api("manage", "Project.deleteProject", { pId: pId, uId: rs.uId })
}

