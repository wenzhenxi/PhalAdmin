import { Api, Session } from "lib"

export const GetAuthorityList = (keyword) => {

        var rs = Session.get("LoginInfo")

        var projectList = Api("manage", "Project.getProjectList", { uId: rs.uId })
        var rs = Api("manage", "Privilege.getPrivilegeList", { uId: rs.uId, keyword: keyword })
        rs.then(function (response) {
            projectList.then(function (res) {
                res.data.forEach(function (projectVal, projectIndex, projectArr) {
                    response.data.forEach(function (val, index, arr) {
                        // 对返回的状态进行过滤
                        if (val.pId == projectVal.pId)
                            val.projectName = projectVal.name
                        if (val.type == 1) {
                            val.type = true
                        } else {
                            val.type = false
                        }
                    })
                })
            })
            return response
        })
        return [rs, projectList]
    }
    //获取项目接口
export const GetApisByPId = (pId) => {
        var rs = Session.get("LoginInfo")
        return Api("manage", "Api.getApiList", { pId: pId, uId: rs.uId })
    }
    //添加权限
export const addAuthority = (name, info, pId, apiList) => {
        var rs = Session.get("LoginInfo")
        return Api("manage", "Privilege.setPrivilege", { name: name, info: info, pId: pId, apiList: apiList, uId: rs.uId })
    }
    //修改权限
export const updateAuthority = (priId, name, info, pId, apiList) => {
    var rs = Session.get("LoginInfo")
    return Api("manage", "Privilege.updatePrivilege", { priId: priId, name: name, info: info, pId: pId, apiList: apiList, uId: rs.uId })
}

//删除权限
export const delAuthority = (priId) => {
        var rs = Session.get("LoginInfo")
        return Api("manage", "Privilege.delPrivilege", { priId: priId, uId: rs.uId })
    }
    //更改启用状态
export const editStatus = (priId, type) => {
    var rs = Session.get("LoginInfo")
    return Api("manage", "Privilege.updatePrivilegeType", { priId: priId, uId: rs.uId, type: type })
}

