import { Api, Session } from "lib"

export const getMenuList = (keyword) => {

        var rs = Session.get("LoginInfo")
        return Api("manage", "Menu.getMenuList", { uId: rs.uId, keyword: keyword })
    }
    //创建菜单
export const addMenu = (name, apiId, pId) => {
        var rs = Session.get("LoginInfo")
        return Api("manage", "Menu.addMenu", { uId: rs.uId, name: name, apiId: apiId, pId: pId })
    }
    //更新角色
export const updateMenu = (mId, name, apiId, pId) => {
        var rs = Session.get("LoginInfo")
        return Api("manage", "Menu.updateMenu", { uId: rs.uId, mId: mId, name: name, apiId: apiId, pId: pId })
    }
    //更改启用状态
export const editStatus = (mId, type) => {
        var rs = Session.get("LoginInfo")
        return Api("manage", "Menu.updateMenuType", { mId: mId, uId: rs.uId, type: type })
    }
    //删除角色
export const delMenu = (mId) => {
        var rs = Session.get("LoginInfo")
        return Api("manage", "Menu.deleteMenu", { mId: mId, uId: rs.uId })
    }
    //获取项目接口
export const getProjectApi = () => {
        var rs = Session.get("LoginInfo")
        return Api("manage", "Project.getProjectList", { uId: rs.uId, keyword: '' })
    }
    //获取项目接口
export const getApisByPId = (pId) => {
    var rs = Session.get("LoginInfo")
    return Api("manage", "Api.getApiList", { pId: pId, uId: rs.uId })
}

