import { Api, Session } from "lib"

export const getApiList = (keyword) => {

        var rs = Session.get("LoginInfo")
        return Api("manage", "Api.getAllApis", { uId: rs.uId, keyword: keyword })
    }
    //创建角色
export const addApi = (name, apiname, pId) => {
        var rs = Session.get("LoginInfo")
        return Api("manage", "Api.addApi", { uId: rs.uId, name: name, apiname: apiname, pId: pId })
    }
    //更新角色
export const updateApi = (aId, name, apiname, pId) => {
        var rs = Session.get("LoginInfo")
        return Api("manage", "Api.updateApi", { uId: rs.uId, aId: aId, name: name, apiname: apiname, pId: pId })
    }
    //更改启用状态
export const editStatus = (aId, type) => {
        var rs = Session.get("LoginInfo")
        return Api("manage", "Api.updateApiType", { aId: aId, uId: rs.uId, type: type })
    }
    //删除角色
export const delApi = (aId) => {
        var rs = Session.get("LoginInfo")
        return Api("manage", "Api.deleteApi", { aId: aId, uId: rs.uId })
    }
    //获取项目
export const getProjects = () => {
    var rs = Session.get("LoginInfo")
    return Api("manage", "Project.getProjectList", { uId: rs.uId })
}

