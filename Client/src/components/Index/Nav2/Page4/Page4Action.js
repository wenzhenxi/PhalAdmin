// 应用初始状态
const state = {
    a: {
        count: 10,
        name: "vuex 测试"
    }

}

// 定义所需的 mutations
const mutations = {
    "todos/INCREMENT": function (state, { i }) {
        console.log(i)
        state.a.count += i
    },
    "todos/DECREMENT": function (state, { i }) {
        console.log(i)
        state.a.count -= i
    },
    "todos/UPDATENAME": function (state,v) {
        state.a.name = v
    }
}

const actions = {
    'todos/INCREMENT': ({ commit }, i) => {
        commit('todos/INCREMENT', i)
    },
    'todos/DECREMENT': ({ commit }, i) => {
        commit('todos/DECREMENT', i)
    }
}

const getters = {
    'todos/getCount': state => {
        console.log(state)
        return state.a.count
    },'todos/getName': state => {
        console.log(state)
        return state.a.name
    },
}

export default {
    state: state,
    mutations: mutations,
    actions: actions,
    getters: getters
}

