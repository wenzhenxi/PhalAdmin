// 加载基础库
import { Api, Session, Tool ,Type} from "lib"

// 应用初始参数
const state = {
  login: {
    logining: false
  }
};

// 定义参数获取方式
const getters = {
  [Type.LOGIN_GET_LOGINING](state) {
    return state.login.logining
  }
};

// 定义所需的 mutations
const mutations = {
    [Type.LOGIN_LOGINING_START](state){
    state.login.logining = true
  },
    [Type.LOGIN_LOGINING_STOP](state){
    state.login.logining = false
  }
};

const actions = {
  [Type.LOGIN](context,_this){
    _this.$refs.user.validate((valid) => {
      if (valid) {
        // 动画开启
        context.commit(Type.LOGIN_LOGINING_START);
        var rs = Api("manage", "Manage.login", { userName: _this.user.account, passWord: Tool.passEnCode(_this.user.password) });
        rs.then(function (response) {
          // 动画关闭
          context.commit(Type.LOGIN_LOGINING_STOP);
          // 对结果进行处理
          if (response.ret == 200) {
            // 暂时没有头像使用默认头像
            response.data.avatar = 'http://pic.w-blog.cn/QQ20170512-152601.png';
            // 如果保存用户名称和密码和状态
            Session.set("LoginInfo", response.data);
            Session.set("account", _this.user.account);
            Session.set("checked", _this.checked);
            if (_this.checked) {
              Session.set("password", _this.user.password);
            }
            // 弹出提示框并且跳转页面
            _this.$message({ message: '登录成功!', type: 'success' });
            _this.$router.replace('/Index/System/Welcome');
          } else {
            _this.$message.error('用户名或密码不正确,请重新输入!');
          }
        }).catch(function (error) {
          // 动画关闭
          context.commit(Type.LOGIN_LOGINING_STOP);
        })

      } else {
        _this.$message({ message: '请检查用户名密码填写是否正常', type: 'warning' })
      };
    })
  }
};

export default {
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters
}

