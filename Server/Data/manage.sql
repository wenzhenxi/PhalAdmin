/*
Navicat MySQL Data Transfer

Source Server         : 139.196.164.174
Source Server Version : 50709
Source Host           : 139.196.164.174:3306
Source Database       : manage

Target Server Type    : MYSQL
Target Server Version : 50709
File Encoding         : 65001

Date: 2016-01-05 13:42:23
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for api
-- ----------------------------
DROP TABLE IF EXISTS `api`;
CREATE TABLE `api` (
  `apiId` int(11) NOT NULL AUTO_INCREMENT COMMENT 'apiId',
  `name` varchar(255) DEFAULT NULL,
  `apiname` varchar(55) DEFAULT NULL COMMENT 'api名称',
  `type` int(11) DEFAULT '1' COMMENT '是否启用',
  `CreateTimeThe` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '自动生成创建时间',
  PRIMARY KEY (`apiId`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8 COMMENT='Api表';

-- ----------------------------
-- Records of api
-- ----------------------------
INSERT INTO `api` VALUES ('1', '帐户列表接口', 'manag.getManagList', '1', '2015-09-16 18:02:44');
INSERT INTO `api` VALUES ('2', '新增账户接口', 'manage.setManag', '1', '2015-09-16 18:02:44');
INSERT INTO `api` VALUES ('3', '编辑账户接口', 'manage.updateManag', '1', '2015-09-16 18:02:44');
INSERT INTO `api` VALUES ('4', '停用/启用帐户接口', 'manage.updateManagType', '1', '2015-09-16 18:02:44');
INSERT INTO `api` VALUES ('5', '角色列表接口', 'role.getRoleList', '1', '2015-09-16 18:02:44');
INSERT INTO `api` VALUES ('6', '新增角色接口', 'role.setRole', '1', '2015-09-16 18:02:44');
INSERT INTO `api` VALUES ('7', '编辑角色接口', 'role.updateRole', '1', '2015-09-16 18:02:44');
INSERT INTO `api` VALUES ('8', '停用/启用角色接口', 'role.updateRoleType', '1', '2015-09-16 18:02:44');
INSERT INTO `api` VALUES ('9', '权限列表接口', 'privilege.getPrivilegeList', '1', '2015-09-16 18:02:44');
INSERT INTO `api` VALUES ('10', '权限新增接口', 'privilege.setPrivilege', '1', '2015-09-16 18:02:45');
INSERT INTO `api` VALUES ('11', '权限编辑接口', 'privilege.updatePrivilege', '1', '2015-09-16 18:02:45');
INSERT INTO `api` VALUES ('12', '权限停用/启用接口', 'privilege.updatePrivilegeType', '1', '2015-09-16 18:02:45');
INSERT INTO `api` VALUES ('13', '删除账户接口', 'manage.deleteManage', '1', '2015-09-16 18:02:45');

-- ----------------------------
-- Table structure for api_menu
-- ----------------------------
DROP TABLE IF EXISTS `api_menu`;
CREATE TABLE `api_menu` (
  `apiId` int(11) NOT NULL DEFAULT '0' COMMENT 'apiID',
  `mId` int(11) NOT NULL DEFAULT '0' COMMENT '菜单ID',
  PRIMARY KEY (`apiId`,`mId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='菜单api关联表';

-- ----------------------------
-- Records of api_menu
-- ----------------------------
INSERT INTO `api_menu` VALUES ('1', '3');
INSERT INTO `api_menu` VALUES ('5', '2');
INSERT INTO `api_menu` VALUES ('9', '1');

-- ----------------------------
-- Table structure for manage
-- ----------------------------
DROP TABLE IF EXISTS `manage`;
CREATE TABLE `manage` (
  `adminId` int(11) NOT NULL AUTO_INCREMENT COMMENT '管理者ID',
  `userName` varchar(55) DEFAULT NULL COMMENT '登录名称',
  `passWord` varchar(32) DEFAULT NULL COMMENT '登录密码 MD5',
  `name` varchar(55) DEFAULT NULL COMMENT '管理者姓名',
  `phone` varchar(11) DEFAULT NULL COMMENT '电话号码',
  `email` varchar(255) DEFAULT NULL COMMENT '邮箱',
  `resetPassWordKey` varchar(255) DEFAULT NULL COMMENT '找回密码KEY',
  `resetPassWordTime` varchar(255) DEFAULT NULL COMMENT '找回密码触发时间',
  `passwordState` tinyint(4) DEFAULT '1' COMMENT '密码状态0 正常状态 1未重新设置密码',
  `type` tinyint(4) DEFAULT '1' COMMENT '是否启用',
  `CreateTimeThe` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '自动生成创建时间',
  PRIMARY KEY (`adminId`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='用户管理表';

-- ----------------------------
-- Records of manage
-- ----------------------------
INSERT INTO `manage` VALUES ('1', 'admin', '5f3bfa0fdda064894b73cdfd3ddb4fb1', '喵了个咪', '18374857844', '591235675@qq.com', '', '0', 0, '1', '2015-09-16 17:31:58');

-- ----------------------------
-- Table structure for manage_role
-- ----------------------------
DROP TABLE IF EXISTS `manage_role`;
CREATE TABLE `manage_role` (
  `adminId` int(11) NOT NULL DEFAULT '0' COMMENT '管理员ID',
  `rId` int(11) NOT NULL DEFAULT '0' COMMENT '角色ID',
  PRIMARY KEY (`adminId`,`rId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色关联用户表';

-- ----------------------------
-- Records of manage_role
-- ----------------------------
INSERT INTO `manage_role` VALUES ('1', '4');

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `mId` int(11) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `name` varchar(55) DEFAULT NULL COMMENT '菜单名称',
  `icon` varchar(255) DEFAULT NULL,
  `url` varchar(2555) DEFAULT NULL COMMENT '请求地址',
  `type` int(11) DEFAULT '1' COMMENT '是否启用',
  `CreateTimeThe` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '自动生成创建时间',
  PRIMARY KEY (`mId`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COMMENT='菜单表';

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES ('1', '权限', '', '', '1', '2015-09-16 18:09:59');
INSERT INTO `menu` VALUES ('2', '角色', '', '', '1', '2015-09-16 18:09:59');
INSERT INTO `menu` VALUES ('3', '账户', '', '', '1', '2015-09-16 18:09:59');

-- ----------------------------
-- Table structure for privilege
-- ----------------------------
DROP TABLE IF EXISTS `privilege`;
CREATE TABLE `privilege` (
  `priId` int(11) NOT NULL AUTO_INCREMENT COMMENT '权限ID',
  `name` varchar(55) DEFAULT NULL COMMENT '权限名称',
  `info` varchar(255) DEFAULT NULL COMMENT '权限说明',
  `type` int(11) DEFAULT '1' COMMENT '是否启用',
  `CreateTimeThe` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '自动生成创建时间',
  PRIMARY KEY (`priId`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COMMENT='权限表';

-- ----------------------------
-- Records of privilege
-- ----------------------------
INSERT INTO `privilege` VALUES ('1', '账户查看权限', '可以进行账户查看', '1', '2015-09-16 18:23:11');
INSERT INTO `privilege` VALUES ('2', '账户增删改权限', '可以进行账户的增删改(需要现拥有查看权限)', '1', '2015-09-16 18:23:11');
INSERT INTO `privilege` VALUES ('3', '角色列表权限', '可以进行角色查看', '1', '2015-09-16 18:23:12');
INSERT INTO `privilege` VALUES ('4', '角色增删改权限', '可以进行角色的增删改(需要现拥有查看权限)', '1', '2015-09-16 18:23:12');
INSERT INTO `privilege` VALUES ('5', '权限列表权限', '可以进行权限查看', '1', '2015-09-16 18:23:12');
INSERT INTO `privilege` VALUES ('6', '权限增删改权限', '可以进行权限的增删改(需要现拥有查看权限)', '1', '2015-09-16 18:23:12');

-- ----------------------------
-- Table structure for privilege_api
-- ----------------------------
DROP TABLE IF EXISTS `privilege_api`;
CREATE TABLE `privilege_api` (
  `priId` int(11) NOT NULL DEFAULT '0' COMMENT '权限ID',
  `apiId` int(11) NOT NULL DEFAULT '0' COMMENT 'APIID',
  PRIMARY KEY (`priId`,`apiId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='api权限关联表';

-- ----------------------------
-- Records of privilege_api
-- ----------------------------
INSERT INTO `privilege_api` VALUES ('1', '1');
INSERT INTO `privilege_api` VALUES ('2', '2');
INSERT INTO `privilege_api` VALUES ('2', '3');
INSERT INTO `privilege_api` VALUES ('2', '4');
INSERT INTO `privilege_api` VALUES ('3', '5');
INSERT INTO `privilege_api` VALUES ('4', '6');
INSERT INTO `privilege_api` VALUES ('4', '7');
INSERT INTO `privilege_api` VALUES ('4', '8');
INSERT INTO `privilege_api` VALUES ('5', '9');
INSERT INTO `privilege_api` VALUES ('6', '10');
INSERT INTO `privilege_api` VALUES ('6', '11');
INSERT INTO `privilege_api` VALUES ('6', '12');
INSERT INTO `privilege_api` VALUES ('2', '13');

-- ----------------------------
-- Table structure for project
-- ----------------------------
DROP TABLE IF EXISTS `project`;
CREATE TABLE `project` (
  `pId` int(11) NOT NULL AUTO_INCREMENT COMMENT '项目ID',
  `name` varchar(55) DEFAULT NULL COMMENT '项目名称',
  `icon` varchar(255) DEFAULT NULL COMMENT '项目图标',
  `type` int(11) DEFAULT '1' COMMENT '是否启用',
  `CreateTimeThe` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`pId`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of project
-- ----------------------------
INSERT INTO `project` VALUES ('1', 'manage', '', '1', '2015-09-16 17:45:02');

-- ----------------------------
-- Table structure for project_api
-- ----------------------------
DROP TABLE IF EXISTS `project_api`;
CREATE TABLE `project_api` (
  `pId` int(11) NOT NULL DEFAULT '0' COMMENT '项目id',
  `apiId` int(11) NOT NULL DEFAULT '0' COMMENT 'APIID',
  PRIMARY KEY (`pId`,`apiId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of project_api
-- ----------------------------
INSERT INTO `project_api` VALUES ('1', '1');
INSERT INTO `project_api` VALUES ('1', '2');
INSERT INTO `project_api` VALUES ('1', '3');
INSERT INTO `project_api` VALUES ('1', '4');
INSERT INTO `project_api` VALUES ('1', '5');
INSERT INTO `project_api` VALUES ('1', '6');
INSERT INTO `project_api` VALUES ('1', '7');
INSERT INTO `project_api` VALUES ('1', '8');
INSERT INTO `project_api` VALUES ('1', '9');
INSERT INTO `project_api` VALUES ('1', '10');
INSERT INTO `project_api` VALUES ('1', '11');
INSERT INTO `project_api` VALUES ('1', '12');
INSERT INTO `project_api` VALUES ('1', '13');

-- ----------------------------
-- Table structure for project_menu
-- ----------------------------
DROP TABLE IF EXISTS `project_menu`;
CREATE TABLE `project_menu` (
  `pId` int(11) NOT NULL DEFAULT '0' COMMENT '项目名称',
  `mId` int(11) NOT NULL DEFAULT '0' COMMENT '菜单ID',
  PRIMARY KEY (`pId`,`mId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of project_menu
-- ----------------------------
INSERT INTO `project_menu` VALUES ('1', '1');
INSERT INTO `project_menu` VALUES ('1', '2');
INSERT INTO `project_menu` VALUES ('1', '3');

-- ----------------------------
-- Table structure for project_privilege
-- ----------------------------
DROP TABLE IF EXISTS `project_privilege`;
CREATE TABLE `project_privilege` (
  `priId` int(11) NOT NULL DEFAULT '0' COMMENT '权限ID',
  `pId` int(11) NOT NULL DEFAULT '0' COMMENT '项目ID',
  PRIMARY KEY (`pId`,`priId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of project_privilege
-- ----------------------------
INSERT INTO `project_privilege` VALUES ('1', '1');
INSERT INTO `project_privilege` VALUES ('2', '1');
INSERT INTO `project_privilege` VALUES ('3', '1');
INSERT INTO `project_privilege` VALUES ('4', '1');
INSERT INTO `project_privilege` VALUES ('5', '1');
INSERT INTO `project_privilege` VALUES ('6', '1');

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `rId` int(11) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `name` varchar(55) DEFAULT NULL COMMENT '角色名称',
  `info` varchar(255) DEFAULT NULL COMMENT '角色说明',
  `type` int(11) DEFAULT '1' COMMENT '是否启用',
  `CreateTimeThe` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '自动生成创建时间',
  PRIMARY KEY (`rId`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='角色管理表';

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES ('1', '权限管理员', '权限管理', '1', '2015-09-16 18:14:51');
INSERT INTO `role` VALUES ('2', '角色管理员', '角色管理', '1', '2015-09-16 18:14:51');
INSERT INTO `role` VALUES ('3', '账户管理员', '账户管理', '1', '2015-09-16 18:14:51');
INSERT INTO `role` VALUES ('4', 'admin', 'admin', '1', '2015-09-16 18:18:13');

-- ----------------------------
-- Table structure for role_privilege
-- ----------------------------
DROP TABLE IF EXISTS `role_privilege`;
CREATE TABLE `role_privilege` (
  `rId` int(11) NOT NULL DEFAULT '0' COMMENT '角色ID',
  `priId` int(11) NOT NULL DEFAULT '0' COMMENT '权限ID',
  PRIMARY KEY (`rId`,`priId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色关联权限表';

-- ----------------------------
-- Records of role_privilege
-- ----------------------------
INSERT INTO `role_privilege` VALUES ('1', '5');
INSERT INTO `role_privilege` VALUES ('1', '6');
INSERT INTO `role_privilege` VALUES ('2', '3');
INSERT INTO `role_privilege` VALUES ('2', '4');
INSERT INTO `role_privilege` VALUES ('3', '1');
INSERT INTO `role_privilege` VALUES ('3', '2');
INSERT INTO `role_privilege` VALUES ('4', '1');
INSERT INTO `role_privilege` VALUES ('4', '2');
INSERT INTO `role_privilege` VALUES ('4', '3');
INSERT INTO `role_privilege` VALUES ('4', '4');
INSERT INTO `role_privilege` VALUES ('4', '5');
INSERT INTO `role_privilege` VALUES ('4', '6');
