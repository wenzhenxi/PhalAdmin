<?php
/**
 * 翻译说明：
 * 1、带大括号的保留原来写法，如：{name}，会被系统动态替换
 * 2、没在以下出现的，可以自行追加
 * @author dogstar <chanzonghuang@gmail.com> 2015-02-09
 */

return array(
    'Hi {name}, welcome to use PhalApi!'          => '{name}您好，欢迎使用PhalApi！',
    'user not exists'                             => '用户不存在!',
    'Admin User Do not stop'                      => 'Admin用户不可停用!',
    'userName existing'                           => '用户名已存在,请更换用户名称!',
    'userEmail existing'                          => '邮箱已存在,请更换邮箱!',
    'Admin User Do not Delete'                    => 'Admin用户不可删除!',
    'Email Is No'                                 => '用户邮箱不存在!',
    'System Error'                                => "系统异常!",
    'Email Send Error'                            => "邮件发送失败!",

    /***************************以下为邮件模版***************************************/
    'Email ResetPassWord{url}{name}{email}{date}' => file_get_contents(API_ROOT . "/Language/zh_cn/tpl/ResetPassWord.html"),
);