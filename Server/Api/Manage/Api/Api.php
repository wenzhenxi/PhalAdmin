<?php

/**
 * API接口
 * @author: 喵了个咪  <wenzhenxi@vip.qq.com>    2015-9-16
 */
class Api_Api extends PhalApi_Api {

    public function getRules() {

        return array(
            //通过项目名获取接口清单
            'getApiList' => array(
                'uId' => array('name' => 'uId', 'require' => true),
                'pId' => array('name' => 'pId', 'require' => true),
            ),
            //获取所有接口
            'getAllApis' => array(
                'uId' => array('name' => 'uId', 'require' => true),
            ),
            //添加接口
            'addApi' => array(
                'uId' => array('name' => 'uId', 'require' => true),
                'name' => array('name' => 'name', 'require' => true),
                'apiname' => array('name' => 'apiname', 'require' => true),
                'pId' => array('name' => 'pId', 'require' => true),
            ),
            //修改接口
            'updateApi' => array(
                'uId' => array('name' => 'uId', 'require' => true),
                'aId' => array('name' => 'aId', 'require' => true),
                'name' => array('name' => 'name', 'require' => true),
                'apiname' => array('name' => 'apiname', 'require' => true),
                'pId' => array('name' => 'pId', 'require' => true),
            ),
            //修改启用状态
            'updateApiType' => array(
                'uId' => array('name' => 'uId', 'require' => true),
                'aId' => array('name' => 'aId', 'require' => true),
                'type' => array('name' => 'type', 'require' => true, 'type' => 'int', 'min'=> 0, 'max' => 1),
            ),
            //删除接口
            'deleteApi' => array(
                'uId' => array('name' => 'uId', 'require' => true),
                'aId' => array('name' => 'aId', 'require' => true),
            ),
        );
    }

    /**
     * 通过项目名获取接口清单
     */
    public function getApiList() {

        //验证管理员账号
        $Domain_Manage = new Domain_Manage();
        $Domain_Manage->checkadminId($this->uId);

        //通过项目名获取接口清单
        $Domain_Api = new Domain_Api();
        return $Domain_Api->getApiList($this->uId, $this->pId);
    }

    /**
     * 获取所有接口
     */
    public function getAllApis(){
        //验证管理员账号
        $Domain_Manage = new Domain_Manage();
        $Domain_Manage->checkadminId($this->uId);
        //权限验证
        $Domain_Validation = new Domain_Validation();
        $Domain_Validation->validation('Api.getAllApis', $this->uId);
        $Domain_Api = new Domain_Api();
        return $Domain_Api->getAllApis();
    }

    /**
     * 添加接口
     */
    public function addApi(){
        //验证管理员账号
        $Domain_Manage = new Domain_Manage();
        $Domain_Manage->checkadminId($this->uId);
        //权限验证
        $Domain_Validation = new Domain_Validation();
        $Domain_Validation->validation('Api.addApi', $this->uId);
        $Domain_Api = new Domain_Api();
        $Domain_Api->addApi($this->name, $this->apiname, $this->pId);
    }

    /**
     * 修改接口
     */
    public function updateApi(){
        //验证管理员账号
        $Domain_Manage = new Domain_Manage();
        $Domain_Manage->checkadminId($this->uId);
        //权限验证
        $Domain_Validation = new Domain_Validation();
        $Domain_Validation->validation('Api.updateApi', $this->uId);
        $Domain_Api = new Domain_Api();
        $Domain_Api->updateApi($this->aId, $this->name, $this->apiname, $this->pId);
    }

    /**
     * 修改启用状态
     */
    public function updateApiType(){
        //验证管理员账号
        $Domain_Manage = new Domain_Manage();
        $Domain_Manage->checkadminId($this->uId);
        //权限验证
        $Domain_Validation = new Domain_Validation();
        $Domain_Validation->validation('Api.updateApiType', $this->uId);
        $Domain_Api = new Domain_Api();
        $Domain_Api->updateApiType($this->aId, $this->type);
    }

    /**
     * 删除接口
     */
    public function deleteApi(){
        //验证管理员账号
        $Domain_Manage = new Domain_Manage();
        $Domain_Manage->checkadminId($this->uId);
        //权限验证
        $Domain_Validation = new Domain_Validation();
        $Domain_Validation->validation('Api.deleteApi', $this->uId);
        $Domain_Api = new Domain_Api();
        $Domain_Api->deleteApi($this->aId);
    }
}


