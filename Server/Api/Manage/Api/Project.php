<?php

/**
 * 项目接口
 * @author: 喵了个咪  <wenzhenxi@vip.qq.com>    2015-9-16
 */
class Api_Project extends PhalApi_Api {

    public function getRules() {

        return array(
            //获取项目清单
            'getProjectList' => array(
                'uId' => array('name' => 'uId', 'require' => true),
            ),
            //新增项目
            'addProject' => array(
                'uId' => array('name' => 'uId', 'require' => true),
                'name' => array('name' => 'name', 'require' => true),
            ),
            //更改启用状态
            'updateProjectType' => array(
                'uId' => array('name' => 'uId', 'require' => true),
                'pId' => array('name' => 'pId', 'require' => true),
                'type' => array('name' => 'type', 'require' => true, 'type' => 'int', 'min'=> 0, 'max' => 1),
            ),
            //编辑项目
            'updateProject' => array(
                'uId' => array('name' => 'uId', 'require' => true),
                'pId' => array('name' => 'pId', 'require' => true),
                'name' => array('name' => 'name', 'require' => true),
            ),
            //编辑项目
            'deleteProject' => array(
                'uId' => array('name' => 'uId', 'require' => true),
                'pId' => array('name' => 'pId', 'require' => true),
            ),
        );
    }

    /**
     * 获取项目清单
     */
    public function getProjectList() {

        //验证管理员账号
        $Domain_Manage = new Domain_Manage();
        $Domain_Manage->checkadminId($this->uId);
        //权限验证
        $Domain_Validation = new Domain_Validation();
        $Domain_Validation->validation('Project.getProjectList', $this->uId);
        //获取项目清单
        $Domain_Project = new Domain_Project();
        return $Domain_Project->getProjectList();
    }

    /**
     * 新增项目
     */
    public function addProject(){
        //验证管理员账号
        $Domain_Manage = new Domain_Manage();
        $Domain_Manage->checkadminId($this->uId);
        //权限验证
        $Domain_Validation = new Domain_Validation();
        $Domain_Validation->validation('Project.addProject', $this->uId);

        $Doamin_Project = new Domain_Project();
        $Doamin_Project->addProject($this->name);
    }

    /**
     * 修改项目
     */
    public function updateProject(){
        //验证管理员账号
        $Domain_Manage = new Domain_Manage();
        $Domain_Manage->checkadminId($this->uId);
        //权限验证
        $Domain_Validation = new Domain_Validation();
        $Domain_Validation->validation('Project.updateProject', $this->uId);

        $Doamin_Project = new Domain_Project();
        $Doamin_Project->updateProject($this->pId, $this->name);
    }

    /**
     * 更改启用状态
     */
    public function updateProjectType(){
        //验证管理员账号
        $Domain_Manage = new Domain_Manage();
        $Domain_Manage->checkadminId($this->uId);
        //权限验证
        $Domain_Validation = new Domain_Validation();
        $Domain_Validation->validation('Project.updateProjectType', $this->uId);

        $Doamin_Project = new Domain_Project();
        $Doamin_Project->updateProjectType($this->pId, $this->type);
    }

    /**
     * 删除项目
     */
    public function deleteProject(){
        //验证管理员账号
        $Domain_Manage = new Domain_Manage();
        $Domain_Manage->checkadminId($this->uId);
        //权限验证
        $Domain_Validation = new Domain_Validation();
        $Domain_Validation->validation('Project.deleteProject', $this->uId);

        $Doamin_Project = new Domain_Project();
        $Doamin_Project->deleteProject($this->pId);
    }
}


