<?php

/**
 * 权限接口
 * @author: 喵了个咪  <wenzhenxi@vip.qq.com>    2015-9-16
 */
class Api_Privilege extends PhalApi_Api {

    public function getRules() {

        return array(
            //权限简介列表
            'getSimplePrivilegeList' => array(
                'uId' => array('name' => 'uId', 'require' => true),
            ),
            //权限列表
            'getPrivilegeList'       => array(
                'uId' => array('name' => 'uId', 'require' => true),
                'keyword' => array('name' => 'keyword'),
            ),
            //新增权限
            'setPrivilege'           => array(
                'uId'     => array('name' => 'uId', 'require' => true),
                'name'    => array('name' => 'name', 'require' => true, 'min' => 1),
                'info'    => array('name' => 'info', 'require' => true, 'max' => 300),
                'pId'     => array('name' => 'pId', 'require' => true, 'min' => 1),
                'apiList' => array('name' => 'apiList', 'require' => true),
            ),
            //编辑权限
            'updatePrivilege'        => array(
                'priId'   => array('name' => 'priId', 'require' => true),
                'uId'     => array('name' => 'uId', 'require' => true),
                'name'    => array('name' => 'name', 'require' => true, 'min' => 1),
                'info'    => array('name' => 'info', 'require' => true, 'max' => 300),
                'pId'     => array('name' => 'pId', 'require' => true, 'min' => 1),
                'apiList' => array('name' => 'apiList', 'require' => true),
            ),
            //停用/启用权限
            'updatePrivilegeType'    => array(
                'priId' => array('name' => 'priId', 'require' => true),
                'uId'   => array('name' => 'uId', 'require' => true),
                'type'  => array('name' => 'type', 'require' => true),
            ),
            //删除权限
            'delPrivilege'          => array(
                'priId' => array('name' => 'priId', 'require' => true),
                'uId'   => array('name' => 'uId', 'require' => true),
            ),
        );
    }

    /**
     * 停用/启用权限
     */
    public function updatePrivilegeType() {

        //验证管理员账号
        $Domain_Manage = new Domain_Manage();
        $Domain_Manage->checkadminId($this->uId);
        //权限验证
        $Domain_Validation = new Domain_Validation();
        $Domain_Validation->validation('privilege.updatePrivilegeType', $this->uId);
        //修改权限信息
        $Domain_Privilege = new Domain_Privilege();
        $Domain_Privilege->updatePrivilegeType($this->priId, $this->type);
    }

    /**
     * 权限简介列表(分项目)
     */
    public function getSimplePrivilegeList() {

        //验证管理员账号
        $Domain_Manage = new Domain_Manage();
        $Domain_Manage->checkadminId($this->uId);
        //获取项目清单
        $Domain_Project = new Domain_Project();
        $projectList    = $Domain_Project->getProjectIdList();
        //权限简介列表(分项目)
        $Domain_Privilege = new Domain_Privilege();
        return $Domain_Privilege->getSimplePrivilegeList($projectList);
    }

    /**
     * 权限列表
     */
    public function getPrivilegeList() {

        //验证管理员账号
        $Domain_Manage = new Domain_Manage();
        $Domain_Manage->checkadminId($this->uId);
        //权限验证
        $Domain_Validation = new Domain_Validation();
        $Domain_Validation->validation('privilege.getPrivilegeList', $this->uId);

        $Domain_Privilege = new Domain_Privilege();
        return $Domain_Privilege->getPrivilegeList($this->keyword);
    }

    /**
     * 添加权限
     */
    public function setPrivilege() {

        //验证管理员账号
        $Domain_Manage = new Domain_Manage();
        $Domain_Manage->checkadminId($this->uId);
        //权限验证
        $Domain_Validation = new Domain_Validation();
        $Domain_Validation->validation('privilege.setPrivilege', $this->uId);
        $this->apiList = DI()->base->myJsonDecode($this->apiList);
        if (!is_array($this->apiList)) {
            throw new PhalApi_Exception_BadRequest(T('apiList No array'));
        } else {
            $this->apiList = array_unique($this->apiList);
        }
        //验证项目是否存在
        $Domain_Project = new Domain_Project();
        $Domain_Project->validationProject($this->pId);

        //添加权限
        $Domain_Privilege = new Domain_Privilege();
        $priId            = $Domain_Privilege->setPrivilege($this->name, $this->info);
        //添加权限和项目的关联
        $Domain_Project->setProjectOrPrivilege($this->pId, $priId);
        //添加权限和API关联
        $Domain_Api = new Domain_Api();
        $Domain_Api->setapiListOrpriId($this->apiList, $priId);
    }

    /**
     * 编辑权限
     */
    public function updatePrivilege() {

        //验证管理员账号
        $Domain_Manage = new Domain_Manage();
        $Domain_Manage->checkadminId($this->uId);
        //权限验证
        $Domain_Validation = new Domain_Validation();
        $Domain_Validation->validation('privilege.updatePrivilege', $this->uId);
        $this->apiList = DI()->base->myJsonDecode($this->apiList);
        if (!is_array($this->apiList)) {
            throw new PhalApi_Exception_BadRequest(T('apiList No array'));
        } else {
            $this->apiList = array_unique($this->apiList);
        }
        //验证项目是否存在
        $Domain_Project = new Domain_Project();
        $Domain_Project->validationProject($this->pId);
        //判断权限是否存在
        $Domain_Privilege = new Domain_Privilege();
        $Domain_Privilege->validationPriId($this->priId);

        //添加权限和项目的关联
        $Domain_Project->setProjectOrPrivilege($this->pId, $this->priId);

        //修改权限信息
        $Domain_Privilege->updatePrivilege($this->priId, $this->name, $this->info);

        $Domain_Api = new Domain_Api();
        //删除原有关联
        $Domain_Api->delapiListOrpriId($this->priId);
        //添加权限和API关联
        $Domain_Api->setapiListOrpriId($this->apiList, $this->priId);
    }

    /**
     * 删除权限
     */
    public function delPrivilege(){
        //验证管理员账号
        $Domain_Manage = new Domain_Manage();
        $Domain_Manage->checkadminId($this->uId);
        //权限验证
        $Domain_Validation = new Domain_Validation();
        $Domain_Validation->validation('privilege.delPrivilege', $this->uId);
        //删除
        $Domain_Privilege = new Domain_Privilege();
        $Domain_Privilege->delPrivilege($this->priId);
    }
}


