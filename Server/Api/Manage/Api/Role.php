<?php

/**
 * 角色接口
 * @author: 喵了个咪  <wenzhenxi@vip.qq.com>    2015-9-16
 */
class Api_Role extends PhalApi_Api {

    public function getRules() {

        return array(
            //获取角色简介清单
            'getSimpleRoleList' => array(
                'uId' => array('name' => 'uId', 'require' => true),
            ),
            //创建角色
            'setRole'           => array(
                'uId'           => array('name' => 'uId', 'require' => true),
                'name'          => array('name' => 'name', 'require' => true, 'min' => 1),
                'info'          => array('name' => 'info', 'require' => true),
                'privilegeList' => array('name' => 'privilegeList', 'require' => true),
            ),
            //获取角色简介清单
            'getRoleList'       => array(
                'uId' => array('name' => 'uId', 'require' => true),
                'keyword' => array('name' => 'keyword', 'require' => true),
            ),
            //更新角色
            'updateRole'        => array(
                'uId'           => array('name' => 'uId', 'require' => true),
                'rId'           => array('name' => 'rId', 'require' => true),
                'name'          => array('name' => 'name', 'require' => true, 'min' => 1),
                'info'          => array('name' => 'info', 'require' => true),
                'privilegeList' => array('name' => 'privilegeList', 'require' => true),
            ),
            //停用/启用角色
            'updateRoleType'    => array(
                'uId'  => array('name' => 'uId', 'require' => true),
                'rId'  => array('name' => 'rId', 'require' => true),
                'type' => array('name' => 'type', 'require' => true),
            ),
            //删除角色
            'deleteRole'    => array(
                'uId'  => array('name' => 'uId', 'require' => true),
                'rId'  => array('name' => 'rId', 'require' => true),
            ),
        );
    }

    /**
     * 获取角色简介清单
     */
    public function getSimpleRoleList() {

        //验证管理员账号
        $Domain_Manage = new Domain_Manage();
        $Domain_Manage->checkadminId($this->uId);
        //获取角色简介清单
        $Domain_Role = new Domain_Role();
        return $Domain_Role->getSimpleRoleList();
    }

    /**
     * 获取角色列表
     */
    public function getRoleList() {

        //验证管理员账号
        $Domain_Manage = new Domain_Manage();
        $Domain_Manage->checkadminId($this->uId);
        //权限验证
        $Domain_Validation = new Domain_Validation();
        $Domain_Validation->validation('role.getRoleList', $this->uId);
        //获取角色列表
        $Domain_Role = new Domain_Role();
        return $Domain_Role->getRoleList($this->keyword);
    }

    /**
     * 新增角色
     */
    public function setRole() {

        //验证管理员账号
        $Domain_Manage = new Domain_Manage();
        $Domain_Manage->checkadminId($this->uId);
        //权限验证
        $Domain_Validation = new Domain_Validation();
        $Domain_Validation->validation('role.setRole', $this->uId);
        //验证角色名
        $Domain_Role = new Domain_Role();
        $Domain_Role->validationNmae($this->name);
        //添加角色
        $rId = $Domain_Role->setRole($this->name, $this->info);
        //重新分配角色对于的权限
        $Domain_Privilege = new Domain_Privilege();
        $this->privilegeList = DI()->base->myJsonDecode($this->privilegeList);
        $Domain_Privilege->updatePrivilegeOrRole($rId, $this->privilegeList);
    }

    /**
     * 修改角色
     */
    public function updateRole() {

        //验证管理员账号
        $Domain_Manage = new Domain_Manage();
        $Domain_Manage->checkadminId($this->uId);
        //权限验证
        $Domain_Validation = new Domain_Validation();
        $Domain_Validation->validation('role.updateRole', $this->uId);
        $this->privilegeList = DI()->base->myJsonDecode($this->privilegeList);
        if (!is_array($this->privilegeList)) {
            throw new PhalApi_Exception_BadRequest(T('privilegeList No array'));
        } else {
            $this->privilegeList = array_unique($this->privilegeList);
        }

        //验证角色名
        $Domain_Role = new Domain_Role();
        $Domain_Role->validationNmaeupdate($this->name, $this->rId);
        //修改角色信息
        $Domain_Role->uodateRole($this->rId, $this->name, $this->info);

        //重新分配角色对于的权限
        $Domain_Privilege = new Domain_Privilege();
        $Domain_Privilege->updatePrivilegeOrRole($this->rId, $this->privilegeList);
    }

    /**
     * 停用/启用角色
     */
    public function updateRoleType() {

        //验证管理员账号
        $Domain_Manage = new Domain_Manage();
        $Domain_Manage->checkadminId($this->uId);
        //权限验证
        $Domain_Validation = new Domain_Validation();
        $Domain_Validation->validation('role.updateRoleType', $this->uId);
        $Domain_Role = new Domain_Role();
        $Domain_Role->uodateRoleType($this->rId, $this->type);
    }

    /**
     * 删除角色
     */
    public function deleteRole(){
        //验证管理员账号
        $Domain_Manage = new Domain_Manage();
        $Domain_Manage->checkadminId($this->uId);
        //权限验证
        $Domain_Validation = new Domain_Validation();
        $Domain_Validation->validation('role.deleteRole', $this->uId);
        $Domain_Role = new Domain_Role();
        $Domain_Role->deleteRole($this->rId);
    }
}


