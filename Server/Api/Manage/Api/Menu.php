<?php

class Api_Menu extends PhalApi_Api {

    public function getRules() {

        return array(
            //获取菜单
            'getMenuList' => array(
                'uId' => array('name' => 'uId', 'require' => true),
                'keyword' => array('name' => 'keyword', 'require' => true),
            ),
            //添加菜单
            'addMenu' => array(
                'uId' => array('name' => 'uId', 'require' => true),
                'name' => array('name' => 'name', 'require' => true),
                'apiId' => array('name' => 'apiId', 'require' => true),
                'pId' => array('name' => 'pId', 'require' => true),
            ),
            //修改菜单
            'updateMenu' => array(
                'uId' => array('name' => 'uId', 'require' => true),
                'mId' => array('name' => 'mId', 'require' => true),
                'name' => array('name' => 'name', 'require' => true),
                'apiId' => array('name' => 'apiId', 'require' => true),
                'pId' => array('name' => 'pId', 'require' => true),
            ),
            //修改启用状态
            'updateMenuType' => array(
                'uId' => array('name' => 'uId', 'require' => true),
                'mId' => array('name' => 'mId', 'require' => true),
                'type' => array('name' => 'type', 'require' => true, 'type' => 'int', 'min'=> 0, 'max' => 1),
            ),
            //删除菜单
            'deleteMenu' => array(
                'uId' => array('name' => 'uId', 'require' => true),
                'mId' => array('name' => 'mId', 'require' => true),
            ),
        );
    }

    /**
     * 获取菜单
     */
    public function getMenuList() {

        //验证管理员账号
        $Domain_Manage = new Domain_Manage();
        $Domain_Manage->checkadminId($this->uId);

        //权限验证
        $Domain_Validation = new Domain_Validation();
        $Domain_Validation->validation('Menu.getMenuList', $this->uId);

        //通过项目名获取接口清单
        $Domain_Menu = new Domain_Menu();
        return $Domain_Menu->getMenuList($this->keyword);
    }

    /**
     * 添加接口
     */
    public function addMenu(){
        //验证管理员账号
        $Domain_Manage = new Domain_Manage();
        $Domain_Manage->checkadminId($this->uId);

        //权限验证
        $Domain_Validation = new Domain_Validation();
        $Domain_Validation->validation('Menu.addMenu', $this->uId);

        $Domain_Menu = new Domain_Menu();
        $Domain_Menu->addMenu($this->name, $this->apiId, $this->pId);
    }

    /**
     * 修改接口
     */
    public function updateMenu(){
        //验证管理员账号
        $Domain_Manage = new Domain_Manage();
        $Domain_Manage->checkadminId($this->uId);

        //权限验证
        $Domain_Validation = new Domain_Validation();
        $Domain_Validation->validation('Menu.updateMenu', $this->uId);

        $Domain_Menu = new Domain_Menu();
        $Domain_Menu->updateMenu($this->mId, $this->name, $this->apiId, $this->pId);
    }

    /**
     * 修改启用状态
     */
    public function updateMenuType(){
        //验证管理员账号
        $Domain_Manage = new Domain_Manage();
        $Domain_Manage->checkadminId($this->uId);
        //权限验证
        $Domain_Validation = new Domain_Validation();
        $Domain_Validation->validation('Menu.updateMenuType', $this->uId);

        $Domain_Menu = new Domain_Menu();
        $Domain_Menu->updateMenuType($this->mId, $this->type);
    }

    /**
     * 删除接口
     */
    public function deleteMenu(){
        //验证管理员账号
        $Domain_Manage = new Domain_Manage();
        $Domain_Manage->checkadminId($this->uId);
        //权限验证
        $Domain_Validation = new Domain_Validation();
        $Domain_Validation->validation('Menu.deleteMenu', $this->uId);
        $Domain_Menu = new Domain_Menu();
        $Domain_Menu->deleteMenu($this->mId);
    }
}


