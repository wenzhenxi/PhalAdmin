<?php

/**
 * 管理者接口
 * @author: 喵了个咪  <wenzhenxi@vip.qq.com>    2015-9-16
 */
class Api_Manage extends PhalApi_Api {

    public function getRules() {

        return array(
            // 登录
            'login'                  => array(
                'userName' => array('name' => 'userName', 'require' => true, 'min' => 2, 'max' => 55,),
                'passWord' => array('name' => 'passWord', 'require' => true, 'min' => 32, 'max' => 32,),
            ),
            // 修改密码
            'setPassWord'            => array(
                'uId'   => array('name' => 'uId', 'require' => true),
                'oldPw' => array('name' => 'oldPw', 'require' => true, 'min' => 32, 'max' => 32),
                'newPw' => array('name' => 'newPw', 'require' => true, 'min' => 32, 'max' => 32),
            ),
            // 找回密码发送邮件
            'ResetPasswordSendEmail' => array(
                'url'   => array('name' => 'url', 'require' => true),
                'email' => array('name' => 'email', 'require' => true),
            ),
            // 找回密码
            'RetrievePassword'       => array(
                'RPtoken'  => array('name' => 'RPtoken', 'require' => true),
                'passWord' => array('name' => 'passWord', 'require' => true, 'min' => 32, 'max' => 32,),
            ),
            // 修改信息
            'setInfo'                => array(
                'uId'   => array('name' => 'uId', 'require' => true),
                'name'  => array('name' => 'name', 'require' => true, 'min' => 2),
                'phone' => array('name' => 'phone', 'require' => true, 'min' => 11, 'max' => 11),
            ),
            // 帐户列表
            'getManageList'          => array(
                'uId'     => array('name' => 'uId', 'require' => true),
                'keyword' => array('name' => 'keyword')
            ),
            // 新增账户
            'createManage'           => array(
                'uId'      => array('name' => 'uId', 'require' => true),
                'userName' => array('name' => 'userName', 'require' => true, 'min' => 5, 'max' => 55),
                'passWord' => array('name' => 'passWord', 'require' => true, 'min' => 32, 'max' => 32),
                'name'     => array('name' => 'name', 'require' => true, 'min' => 2),
                'phone'    => array('name' => 'phone', 'require' => true, 'min' => 11, 'max' => 11),
                'email'    => array('name' => 'email', 'require' => true),
            ),
            // 编辑账户
            'updateManageInfo'       => array(
                'uId'   => array('name' => 'uId', 'require' => true),
                'ouId'  => array('name' => 'ouId', 'require' => true),
                'name'  => array('name' => 'name', 'require' => true, 'min' => 2),
                'phone' => array('name' => 'phone', 'require' => true, 'min' => 11, 'max' => 11),
                'email' => array('name' => 'email', 'require' => true),
            ),
            // 停用/启用帐户
            'updateManageType'       => array(
                'uId'  => array('name' => 'uId', 'require' => true),
                'ouId' => array('name' => 'ouId', 'require' => true),
                'type' => array('name' => 'type', 'require' => true),
            ),
            // 删除账户
            'deleteManage'           => array(
                'uId'  => array('name' => 'uId', 'require' => true),
                'ouId' => array('name' => 'ouId', 'require' => true),
            ),
            //编辑用户权限
            'updateRoles'     => array(
                'uId'  => array('name' => 'uId', 'require' => true),
                'ouId'  => array('name' => 'ouId', 'require' => true),
                'roles' => array('name' => 'roles', 'require' => true),
            ),
        );
    }

    /**
     * 账户设置
     * @return int code 默认返回200成功
     */
    public function setInfo() {

        //验证管理员账号
        $Domain_Manage = new Domain_Manage();
        $Domain_Manage->checkadminId($this->uId);
        //修改信息
        $Domain_Manage = new Domain_Manage();
        $Domain_Manage->setInfo($this->uId, $this->name, $this->phone);
    }

    /**
     * 修改密码
     * @return int code 默认返回200成功
     */
    public function setPassWord() {

        //验证管理员账号
        $Domain_Manage = new Domain_Manage();
        $Domain_Manage->checkadminId($this->uId);
        //验证密码
        $Domain_Manage = new Domain_Manage();
        $Domain_Manage->validationPassWord($this->oldPw, $this->uId);
        //修改密码
        $Domain_Manage->setPassWord($this->newPw, $this->uId);
    }

    /**
     * 用户登录
     * @desc 用户登录 返回用户可用菜单 以及可以使用的API
     * @return int uId 管理员用户ID
     * @return string name 管理员用户名称
     * @return string phone 管理员用户手机号码
     * @return array mList 菜单列表(包含mId(菜单ID),icon(菜单图标),url(菜单地址),name(菜单名称),projectName(项目名称),pId(项目ID))
     * @return array apiList 用户可使用Api列表(可以工具列表来对应按钮是否可以点击)
     */
    public function login() {

        //验证管理员用户获取ID
        $Domain_Managers = new Domain_Manage();
        $UserInfo = $Domain_Managers->login($this->userName, $this->passWord);
        $data = array();
        $data['uId'] = $UserInfo['uId'];
        $data['name'] = $UserInfo['name'];
        $data['phone'] = $UserInfo['phone'];
        //通过用户ID获取菜单和apiId列表
        list($mList, $apiList) = $Domain_Managers->getadminIdBymListapiList($UserInfo['uId']);
        $data['mList'] = $mList;
        $data['apiList'] = $apiList;
        return $data;
    }

    /**
     * 找回密码发送邮件
     */
    public function ResetPasswordSendEmail() {

        // 验证邮箱获取用户信息 并且生成邮件key
        $Domain_Manage = new Domain_Manage();
        list($name, $resetPassWordKey) = $Domain_Manage->ResetPassword($this->email);

        $Domain_Email = new Domain_Email();
        $Domain_Email->sendResetPassWordEmail($this->email, $this->url, $name, $resetPassWordKey);
    }

    /*****************************************账户管理**********************************************************/

    /**
     * 帐户列表
     */
    public function getManageList() {

        //验证管理员账号
        $Domain_Manage = new Domain_Manage();
        $Domain_Manage->checkadminId($this->uId);
        //权限验证
        $Domain_Validation = new Domain_Validation();
        $Domain_Validation->validation('manag.getManagList', $this->uId);
        //获取账户列表
        $Domain_Manage = new Domain_Manage();
        return $Domain_Manage->getManagList($this->keyword);
    }

    /**
     * 新增账户
     */
    public function createManage() {

        //验证管理员账号
        $Domain_Manage = new Domain_Manage();
        $Domain_Manage->checkadminId($this->uId);
        //权限验证
        $Domain_Validation = new Domain_Validation();
        $Domain_Validation->validation('manage.setManag', $this->uId);
        //验证用户名以及邮箱是否存在
        $Domain_Manage = new Domain_Manage();
        $Domain_Manage->increaseValidation($this->userName, $this->email);
        //创建用户
        $uId = $Domain_Manage->increaseManage($this->userName, $this->passWord, $this->name, $this->phone, $this->email);
        return $uId;
    }

    /**
     * 编辑账户
     */
    public function updateManageInfo() {

        // 验证管理员账号
        $Domain_Manage = new Domain_Manage();
        $Domain_Manage->checkadminId($this->uId);
        // 权限验证
        $Domain_Validation = new Domain_Validation();
        $Domain_Validation->validation('manage.updateManag', $this->uId);
        // 修改用户验证邮箱,除了自己不能喝别人的邮箱相同
        $Domain_Manage = new Domain_Manage();
        $Domain_Manage->updateManageCheckEmail($this->ouId, $this->email);
        // 修改用户
        $Domain_Manage->updateManage($this->ouId, $this->name, $this->phone, $this->email);
    }

    /**
     * 停用/启用帐户
     */
    public function updateManageType() {

        //验证管理员账号
        $Domain_Manage = new Domain_Manage();
        $Domain_Manage->checkadminId($this->uId);
        //权限验证
        $Domain_Validation = new Domain_Validation();
        $Domain_Validation->validation('manage.updateManagType', $this->uId);

        $Domain_Manage = new Domain_Manage();
        //修改用户
        $Domain_Manage->updateManageType($this->ouId, $this->type);
    }

    /**
     * 删除账户
     */
    public function deleteManage() {

        // 验证管理员账号
        $Domain_Manage = new Domain_Manage();
        $Domain_Manage->checkadminId($this->uId);
        // 权限验证
        $Domain_Validation = new Domain_Validation();
        $Domain_Validation->validation('manage.deleteManage', $this->uId);
        // 删除用户
        $Domain_Manage = new Domain_Manage();
        $Domain_Manage->deleteManage($this->ouId);
    }

    /**
     * 编辑用户权限
     */
    public function updateRoles(){

        // 验证管理员账号
        $Domain_Manage = new Domain_Manage();
        $Domain_Manage->checkadminId($this->uId);
        // 权限验证
        $Domain_Validation = new Domain_Validation();
        $Domain_Validation->validation('manage.updateRoles', $this->uId);
        //编辑权限
        $Domain_Manage = new Domain_Manage();
        $Domain_Manage->updateRoles($this->ouId, $this->roles);
    }
}


