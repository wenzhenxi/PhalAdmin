<?php

/**
 * 开发者Domain 类
 * @author: 喵了个咪  <wenzhenxi@vip.qq.com> 2015-07-23
 */
class Domain_Project {

    /**
     * 获取项目清单
     */
    public function getProjectList() {

        $Model_Project = new Model_Project();
        $Model_ProjectApi = new Model_ProjectApi();
        $Model_Api = new Model_Api();
        $projects = $Model_Project->getProjectList();
        foreach ($projects as $k=>$v){
            $pId = $v['pId'];
            $count = $Model_ProjectApi->getCoutByPId($pId);
            $projects[$k]['apiNum'] = $count;
            $projectApi = $Model_ProjectApi->getDataByPIdFetchPairsAId($pId, 'apiId');
            $aIds = array_keys($projectApi);
            $apis = $Model_Api->getapiIdListByapiList($aIds);
            $projects[$k]['apis'] = $apis;
        }
        return $projects;
    }

    /**
     * 获取项目清单
     */
    public function getProjectIdList() {

        $Model_Project = new Model_Project();
        return $Model_Project->getProjectIdList();
    }

    /**
     * 关联权限和项目
     */
    public function setProjectOrPrivilege($pId, $priId) {

        //删除原有关联
        $this->delProjectOrPrivilege($priId);
        //关联权限ID和项目ID
        $Model_ProjectPrivilege = new Model_ProjectPrivilege();
        $Model_ProjectPrivilege->setpriIdOrProjectId($pId, $priId);
    }

    /**
     * 删除权限和项目关联
     */
    public function delProjectOrPrivilege($priId) {

        $Model_ProjectPrivilege = new Model_ProjectPrivilege();
        $Model_ProjectPrivilege->delpriIdOrProjectId($priId);
    }

    /**
     * 验证项目是否存在
     */
    public function validationProject($pId) {

        //通过名字获取ID
        $Model_Project = new Model_Project();
        $rs            = $Model_Project->getIdByNmae($pId);
        if (!$rs) {
            throw new PhalApi_Exception_BadRequest(T('No Project'));
        }
    }

    /**
     * 添加项目
     */
    public function addProject($name){
        $Model_Project = new Model_Project();
        $rs = $Model_Project->getDataByName($name);
        if($rs)
            throw new PhalApi_Exception_BadRequest(T('project already exists'));
        $rs = $Model_Project->insertData(array('name'=>$name));
        if(!$rs)
            throw new PhalApi_Exception_BadRequest(T('add project failure'));
    }

    /**
     * 修改项目
     */
    public function updateProject($pId, $name){
        $Model_Project = new Model_Project();
        $rs = $Model_Project->getDataByPIdAndName($pId, $name);
        $this->checkPId($pId);
        if($rs)
            throw new PhalApi_Exception_BadRequest(T('project already exists'));
        $rs = $Model_Project->updateDataByPId($pId, array('name'=>$name));
        if($rs === false)
            throw new PhalApi_Exception_BadRequest(T('update project failure'));
    }

    /**
     * 更改启用状态
     */
    public function updateProjectType($pId, $type){
        $this->checkPId($pId);
        if($type == 0)
            $this->isApiExists($pId);
        $Model_Project = new Model_Project();
        $rs = $Model_Project->updateDataByPId($pId, array('type' => $type));
        if($rs === false)
            throw new PhalApi_Exception_BadRequest(T('update status failure'));
    }

    /**
     * 检查项目是否存在
     */
    private function checkPId($pId){
        $Model_Project = new Model_Project();
        $rs = $Model_Project->getIdByNmae($pId);
        if(!$rs)
            throw new PhalApi_Exception_BadRequest(T('project not exists'));
    }

    /**
     * 检查项目是否存在api
     */
    private function isApiExists($pId){
        $Model_ProjectApi = new Model_ProjectApi();
        $rs = $Model_ProjectApi->getpIdByapiIdList($pId);
        if($rs)
            throw new PhalApi_Exception_BadRequest(T('请先删除此项目对应的接口'));
    }

    /**
     * 删除项目
     */
    public function deleteProject($pId){
        $this->checkPId($pId);
        $this->isApiExists($pId);
        $Model_Project = new Model_Project();
        $rs = $Model_Project->deleteDataByPId($pId);
        if(!$rs)
            throw new PhalApi_Exception_BadRequest(T('delete failure'));
    }
}