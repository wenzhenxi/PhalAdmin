<?php

/**
 * 开发者Domain 类
 * @author: 喵了个咪  <wenzhenxi@vip.qq.com> 2015-07-23
 */
class Domain_Api {

    /**
     * 通过项目名获取接口清单
     */
    public function getApiList($uId, $projectId) {
        
        //通过项目的ID 获取有哪些接口
        $Model_ProjectApi = new Model_ProjectApi();
        $rs               = $Model_ProjectApi->getpIdByapiIdList($projectId);
        $apiIdList        = array();
        foreach ($rs as $v) {
            $apiIdList[] = DI()->base->getIndex($v, 'apiId');
        }
        //通过APIId列表获取接口清单
        $Model_Api = new Model_Api();
        $apilist   = $Model_Api->getapiIdListByapiList($apiIdList);
        if (!$apilist) {
            throw new PhalApi_Exception_BadRequest(T('No apiIdList'));
        }
        return $apilist;
    }

    /**
     * 添加权限和API关联
     */
    public function setapiListOrpriId($apiList, $priId) {

        $Model_PrivilegeApi = new Model_PrivilegeApi();
        foreach ($apiList as $v) {
            $Model_PrivilegeApi->insert(array('priId' => $priId, 'apiId' => $v));
        }
    }

    /**
     * 删除权限和API关联
     */
    public function delapiListOrpriId($priId) {

        $Model_PrivilegeApi = new Model_PrivilegeApi();
        $Model_PrivilegeApi->delapiListOrpriId($priId);
    }

    /**
     * 获取所有接口
     */
    public function getAllApis(){

        $Model_Api = new Model_Api();
        $apis = $Model_Api->getAllDataFetchPairs('apiId, name, apiname, type');
        $aIds = array_keys($apis);
        $Model_ApiMenu = new Model_ApiMenu();
        $apiMenus = $Model_ApiMenu->getDataByAIdFetchPairs($aIds, '');
        $Model_Menu = new Model_Menu();
        foreach ($apiMenus as $k=>$v){
            $rs = $Model_Menu->getDataByMId($v['mId'], 'name');
            if($rs)
                $apiMenus[$k]['menu'] = $rs['name'];
        }

        $Model_ProjectApi = new Model_ProjectApi();
        $projectApis = $Model_ProjectApi->getDataByAIdFetchPairsAId($aIds, 'pId');
        $Model_Project = new Model_Project();
        foreach ($projectApis as $k=>$v){
            $project = $Model_Project->getDataByPId($v['pId'], 'name, pId');
            $projectApis[$k]['name'] = $project['name'];
        }

        foreach ($apis as $k=>$v){
            $apis[$k]['menu'] = DI()->base->getIndex(DI()->base->getIndex($apiMenus, $k), 'menu');
            $apis[$k]['project'] = DI()->base->getIndex(DI()->base->getIndex($projectApis, $k), 'name');
            $apis[$k]['pId'] = DI()->base->getIndex(DI()->base->getIndex($projectApis, $k), 'pId');
        }
        return array_values($apis);
    }

    /**
     * 添加接口
     */
    public function addApi($name, $apiName, $pId){

        $this->checkApiname($apiName);
        $Model_Api = new Model_Api();
        $rs = $Model_Api->insertData(array('name'=>$name, 'apiname'=>$apiName));
        if(!$rs)
            throw new PhalApi_Exception_BadRequest('新增接口失败');
        $apiId = $Model_Api->lastInsertId();
        $Model_ProjectApi = new Model_ProjectApi();
        $rs = $Model_ProjectApi->insertData(array('apiId'=>$apiId, 'pId'=>$pId));
        if(!$rs)
            throw new PhalApi_Exception_BadRequest('新增接口失败');
    }

    /**
     * 检验apiname是否重复
     */
    private function checkApiname($apiName, $aId = 0){
        $Model_Api = new Model_Api();
        if($aId){
            $this->checkAId($aId);
            $rs = $Model_Api->getDataByAIdAndApiName($aId, $apiName, 'apiId');
        }else{
            $rs = $Model_Api->getDataByApiName($apiName, 'apiId');
        }
        if($rs)
            throw new PhalApi_Exception_BadRequest('接口重复');
    }

    /**
     * 检验aId是否存在
     */
    private function checkAId($aId){
        $Model_Api = new Model_Api();
        $rs = $Model_Api->getDataByAId($aId);
        if(!$rs)
            throw new PhalApi_Exception_BadRequest('不存在此接口');
    }

    /**
     * 修改接口
     */
    function updateApi($aId, $name, $apiName, $pId){
        $this->checkApiname($apiName, $aId);
        $Model_Api = new Model_Api();
        $rs = $Model_Api->updateDataByAId($aId, array('name'=>$name, 'apiname'=>$apiName));
        if($rs === false)
            throw new PhalApi_Exception_BadRequest('修改接口失败');
        $Model_ProjectApi = new Model_ProjectApi();
        $rs = $Model_ProjectApi->updateDataByAId($aId, array('pId'=>$pId));
        if($rs === false)
            throw new PhalApi_Exception_BadRequest('修改接口失败');
    }

    /**
     * 修改启用状态
     */
    public function updateApiType($aId, $type){
        $this->checkAId($aId);
        $Model_Api = new Model_Api();
        $rs = $Model_Api->updateDataByAId($aId, array('type'=>$type));
        if($rs === false)
            throw new PhalApi_Exception_BadRequest('更改状态失败');
    }

    /**
     * 删除接口
     */
    public function deleteApi($aId){
        $this->checkAId($aId);
        $Model_Api = new Model_Api();
        $rs = $Model_Api->deleteDataByAId($aId);
        if(!$rs)
            throw new PhalApi_Exception_BadRequest('删除接口失败');
        $Model_ProjectApi = new Model_ProjectApi();
        $rs = $Model_ProjectApi->deleteDataByAId($aId);
        if($rs === false)
            throw new PhalApi_Exception_BadRequest('删除接口失败');
        $Model_PrivilegeApi = new Model_PrivilegeApi();
        $rs = $Model_PrivilegeApi->deleteDataByApiId($aId);
        if($rs === false)
            throw new PhalApi_Exception_BadRequest('删除接口失败');
        $Model_ApiMenu = new Model_ApiMenu();
        $rs = $Model_ApiMenu->deleteDataByApiId($aId);
        if($rs === false)
            throw new PhalApi_Exception_BadRequest('删除接口失败');
    }
}