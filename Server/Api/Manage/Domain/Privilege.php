<?php

/**
 * 开发者Domain 类
 * @author: 喵了个咪  <wenzhenxi@vip.qq.com> 2015-07-23
 */
class Domain_Privilege {

    /**
     * 权限简介列表(分项目)
     */
    public function getSimplePrivilegeList($projectList) {

        $SimplePrivilegeList    = array();
        $Model_Privilege        = new Model_Privilege();
        $Model_ProjectPrivilege = new Model_ProjectPrivilege();
        foreach ($projectList as $value) {
            //通过项目ID获取有哪些权限
            $rs              = $Model_ProjectPrivilege->getProjectIdByPrivilegeIdList(DI()->base->getIndex($value, 'pId'));
            $PrivilegeIdList = array();
            foreach ($rs as $v) {
                $PrivilegeIdList[] = DI()->base->getIndex($v, 'priId');
            }
            //权限简介列表
            $SimplePrivilegeList[DI()->base->getIndex($value, 'name')]['privilege']   = $Model_Privilege->getSimplePrivilegeList($PrivilegeIdList);
            $SimplePrivilegeList[DI()->base->getIndex($value, 'name')]['projectname'] = DI()->base->getIndex($value, 'name');
        }
        return array_values($SimplePrivilegeList);
    }

    /**
     * 重新分配角色对于的权限
     */
    public function updatePrivilegeOrRole($rId, $privilegeList) {

        //删除原有角色拥有的权限
        $Model_RolePrivilege = new Model_RolePrivilege();
        $Model_RolePrivilege->delrId($rId);
        foreach ($privilegeList as $v) {
            $Model_RolePrivilege->setrIdOrPrivilegeId($rId, $v);
        }
    }

    /**
     * 权限列表
     */
    public function getPrivilegeList($keyword) {

        $Model_Privilege = new Model_Privilege();
        $PrivilegeList   = $Model_Privilege->getPrivilegeList($keyword);

        foreach ($PrivilegeList as $k => $v) {
            //通过权限ID获取项目ID
            $Model_ProjectPrivilege = new Model_ProjectPrivilege();
            $rs                     = $Model_ProjectPrivilege->getPrivilegeIdByProjectId(DI()->base->getIndex($v, 'priId'));
            //通过项目ID获取项目名称
            $Model_Project = new Model_Project();
            $rs2           = $Model_Project->getIdByNmae(DI()->base->getIndex($rs, 'pId'));

            $PrivilegeList[$k]['pId'] = DI()->base->getIndex($rs2, 'pId');
            //权限ID获取有哪些接口
            $Model_PrivilegeApi = new Model_PrivilegeApi();
            $rs3                = $Model_PrivilegeApi->getpriIdByapiId(DI()->base->getIndex($v, 'priId'));
            $apiIdList          = array();
            foreach ($rs3 as $v) {
                $apiIdList[] = DI()->base->getIndex($v, 'apiId');
            }
            //通过IDList获取权限名称
            $Model_Api                    = new Model_Api();
            $apiList                      = $Model_Api->getapiIdListByapiList($apiIdList);
            $PrivilegeList[$k]['apiList'] = $apiList;
        }
        return $PrivilegeList;
    }

    /**
     * 新建权限
     */
    public function setPrivilege($name, $info) {

        $Model_Privilege = new Model_Privilege();

        $priId = $Model_Privilege->setPrivilege($name, $info);
        if (!$priId) {
            throw new PhalApi_Exception_BadRequest(T('No setPrivilege'));
        }
        return $priId;
    }

    /**
     * 修改权限
     */
    public function updatePrivilege($priId, $name, $info) {

        $Model_Privilege = new Model_Privilege();
        $rs              = $Model_Privilege->updatePrivilege($priId, $name, $info);
        if ($rs === false) {
            throw new PhalApi_Exception_BadRequest(T('No setPrivilege'));
        }
    }

    /**
     * 修改权限
     */
    public function updatePrivilegeType($priId, $type) {

        $Model_Privilege = new Model_Privilege();
        $rs              = $Model_Privilege->updatePrivilegeType($priId, $type);
        if ($rs === false) {
            throw new PhalApi_Exception_BadRequest(T('No setPrivilege'));
        }
    }

    /**
     * 判断权限是否存在
     */
    public function validationPriId($priId) {

        $Model_Privilege = new Model_Privilege();
        $rs              = $Model_Privilege->getSimplePrivilegeList($priId);
        if (!$rs) {
            throw new PhalApi_Exception_BadRequest(T('No PriId'));
        }
    }

    /**
     * 删除权限
     */
    public function delPrivilege($priId){

        $Model_Privilege = new Model_Privilege();
        $rs = $Model_Privilege->deleteDataByPriId($priId);
        if(!$rs)
            throw new PhalApi_Exception_BadRequest('删除失败');
    }
}