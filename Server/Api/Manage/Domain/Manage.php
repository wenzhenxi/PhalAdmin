<?php

/**
 * 开发者Domain 类
 * @author: 喵了个咪  <wenzhenxi@vip.qq.com> 2015-07-23
 */
class Domain_Manage {

    /**
     * 验证密码
     */
    public function validationPassWord($oldPw, $uId) {

        //通过Id获取密码
        $Model_Manage = new Model_Manage();
        $rs = $Model_Manage->getDevelopersById($uId);
        if ($rs['passWord'] != $oldPw) {
            throw new PhalApi_Exception_BadRequest(T('passWord Match'));
        }
    }

    /**
     * 管理员登录
     */
    public function login($username, $password) {

        $Model_Managers = new Model_Manage();
        $adminId = $Model_Managers->getDevelopersByName($username, $password);
        if (!$adminId) {
            throw new PhalApi_Exception_BadRequest(T('No User'));
        }
        if (!$adminId['type']) {
            throw new PhalApi_Exception_BadRequest(T('No User Type'));
        }
        return $adminId;
    }

    /**
     * 获取账户列表
     */
    public function getManagList($keyword) {

        //获取出来所有的账户列表
        $Model_Manage = new Model_Manage();
        $ManagList = $Model_Manage->getManageList($keyword);
        foreach ($ManagList as $k => $v) {
            //通过用户ID获取角色IDlist
            $Model_ManageRole = new Model_ManageRole();
            $rs2 = $Model_ManageRole->getManagIdListByRoleIdList(DI()->base->getIndex($v, 'uId'));
            $RoleIdlist = array();
            foreach ($rs2 as $varule) {
                $RoleIdlist[] = DI()->base->getIndex($varule, 'rId');
            }
            //通过角色Idlist获取角色名称
            //$Model_Role = new Model_Role();
            //$RoleNameList = $Model_Role->getRoleIdlistBuRoleName($RoleIdlist);
            $ManagList[$k]['role'] = $RoleIdlist;
            $ManagList[$k]['type'] = $ManagList[$k]['type'] == 1 ? true : false;
        }
        return $ManagList;
    }

    /**
     * 验证用户信息是否重复
     */
    public function increaseValidation($userName, $email) {

        //验证用户名称是否重复
        if ($userName) {
            $Model_Manage = new Model_Manage();
            $Manage = $Model_Manage->getUserNameById($userName);
            if ($Manage) {
                throw new PhalApi_Exception_BadRequest(T('userName existing'));
            }
        }
        //验证用户邮箱是否重复
        if ($email) {
            $Model_Manage = new Model_Manage();
            $Manage = $Model_Manage->getEmailById($email);
            if ($Manage) {
                throw new PhalApi_Exception_BadRequest(T('userEmail existing'));
            }
        }
    }

    /**
     * 创建用户
     */
    public function increaseManage($userName, $passWord, $name, $phone, $email) {

        //创建用户
        $Model_Manage = new Model_Manage();
        $adminId = $Model_Manage->increaseManage($userName, $passWord, $name, $phone, $email);
        if (!$adminId) {
            throw new PhalApi_Exception_BadRequest(T('No Manage Increase'));
        }
        return $adminId;
    }

    /**
     * 修改用户
     */
    public function updateManage($uId, $name, $phone, $email) {

        //修改用户
        $Model_Manage = new Model_Manage();
        $rs = $Model_Manage->updateManage($uId, $name, $phone, $email);
        if ($rs === false) {
            throw new PhalApi_Exception_BadRequest(T('No updateManage'));
        }
    }

    /**
     * 停用/启用帐户
     */
    public function updateManageType($uId, $type) {

        if ($uId == 1) {
            throw new PhalApi_Exception_BadRequest(T("Admin User Do not stop"));
        }

        //修改用户
        $Model_Manage = new Model_Manage();
        $rs = $Model_Manage->updateManageType($uId, $type);
        if ($rs === false) {
            throw new PhalApi_Exception_BadRequest(T('No updateManage'));
        }
    }

    /**
     * 删除一个用户
     */
    public function deleteManage($uId) {
        if ($uId == 1) {
            throw new PhalApi_Exception_BadRequest(T("Admin User Do not Delete"));
        }
        //删除用户
        $Model_Manage = new Model_Manage();
        $rs = $Model_Manage->deleteManage($uId);
        if ($rs === false) {
            throw new PhalApi_Exception_BadRequest(T('Delete Error'));
        }
    }

    /**
     * 修改密码
     */
    public function setPassWord($newPw, $uId) {

        $Model_Manage = new Model_Manage();
        $rs = $Model_Manage->setPassWord($newPw, $uId);
        if ($rs === false) {
            throw new PhalApi_Exception_BadRequest(T('No setPassWord'));
        }
    }

    /**
     * 修改信息
     */
    public function setInfo($uId, $name, $phone) {

        $Model_Manage = new Model_Manage();
        $rs = $Model_Manage->updateManage2($uId, $name, $phone);
        if ($rs === false) {
            throw new PhalApi_Exception_BadRequest(T('No setInfo'));
        }
    }

    /**
     * 通过用户ID获取菜单和apiId列表
     */
    public function getadminIdBymListapiList($uId) {

        //通过Id获取角色
        $Model_ManageRole = new Model_ManageRole();
        $rs = $Model_ManageRole->getManagIdListByRoleIdList($uId);
        $RoleIdList = array_column($rs, 'rId');

        //过滤角色
        $Model_Role = new Model_Role();
        $rs = $Model_Role->filterId($RoleIdList);
        $RoleIdList = array_column($rs, 'rId');

        //获取权限ID列表
        $Model_RolePrivilege = new Model_RolePrivilege();
        $rs = $Model_RolePrivilege->getpriIdByPrivilegeIdlist($RoleIdList);
        $PrivilegeIdList = array_column($rs, 'priId');

        //过滤权限
        $Model_Privilege = new Model_Privilege();
        $rs = $Model_Privilege->filterId($PrivilegeIdList);
        $PrivilegeIdList = array_column($rs, 'priId');

        //通过权限获得APIListID
        $Model_PrivilegeApi = new Model_PrivilegeApi();
        $rs = $Model_PrivilegeApi->getpriIdByapiId($PrivilegeIdList);
        $ApiIdList = array_column($rs, 'apiId');

        //过滤权限Api
        $Model_Api = new Model_Api();
        $rs = $Model_Api->filterId($ApiIdList);
        $ApiIdList = array_column($rs, 'apiId');

        //通过API获取菜单的ID
        $Model_ApiMenu = new Model_ApiMenu();
        $rs = $Model_ApiMenu->getApiIdByMenuId($ApiIdList);
        $mIdList = array_column($rs, 'mId');

        //获取菜单列表
        $Model_Menu = new Model_Menu();
        $mList = $Model_Menu->getmIdListByList($mIdList);
        foreach ($mList as $k => $v) {
            //通过ID 获取项目名称ID
            $Model_ProjectMenu = new Model_ProjectMenu();
            $pId = $Model_ProjectMenu->getmIdBypId($v['mId']);
            //通过$pId获取项目名
            $Model_Project = new Model_Project();
            $proname = $Model_Project->getIdByNmae($pId['pId']);
            $mList[$k]['projectName'] = $proname['name'];
            $mList[$k]['pId'] = $proname['pId'];
        }
        return array($mList, $ApiIdList);
    }

    /**
     * 验证uId是否存在
     */
    public function checkadminId($uId) {

        $Model_Manage = new Model_Manage();
        $Manage = $Model_Manage->getDevelopersById($uId);
        if (!$Manage) {
            throw new PhalApi_Exception_BadRequest(T('user not exists'), 1);
        }
        return $Manage;
    }

    /**
     * 修改用户验证邮箱,除了自己不能和别人的邮箱相同
     */
    public function updateManageCheckEmail($uId, $email) {

        $Model_Manage = new Model_Manage();
        $Manage = $Model_Manage->updateManageCheckEmail($uId, $email);
        if ($Manage) {
            throw new PhalApi_Exception_BadRequest(T('userEmail existing'));
        }
    }

    /**
     * 验证邮箱获取用户信息 并且生成邮件key
     */
    public function ResetPassword($email) {

        // 验证邮箱获取用户信息
        $Model_Manage = new Model_Manage();
        $userInfo = $Model_Manage->getInfoByEmail($email);
        if (!$userInfo) {
            throw  new PhalApi_Exception_BadRequest(T('User Email Is No'));
        }
        // 生成ResetPassWordKey
        $PhalApi_Tool = new PhalApi_Tool();
        $resetPassWordKey = $PhalApi_Tool->createRandStr(14);
        $rs = $Model_Manage->setResetPassWordKey($userInfo['uId'], $resetPassWordKey);
        if ($rs === false) {
            throw new PhalApi_Exception_BadRequest(T('System Error'));
        }
        return array($userInfo['name'], $resetPassWordKey);
    }

	/**
     * 编辑用户权限
     */
    public function updateRoles($uId, $roles){
        $roles = DI()->base->myJsonDecode($roles);
        if(!is_array($roles))
            throw new PhalApi_Exception_BadRequest('roles格式错误');
        $Domain_Role = new Domain_Role();
        $Domain_Role->setAdminIdRIdList($uId, $roles);
    }

}