<?php

/**
 * 开发者Domain 类
 * @author: 喵了个咪  <wenzhenxi@vip.qq.com> 2015-07-23
 */
class Domain_Menu {

    /**
     * 获取菜单
     */
    public function getMenuList($keyword) {
        $Model_Menu = new Model_Menu();
        $menus = $Model_Menu->getAllDataFetchPairs('mId, name, type', $keyword);
        $mIds = array_keys($menus);
        $Model_ApiMenu = new Model_ApiMenu();
        $apiMenus = $Model_ApiMenu->getDataByMIdFetchPairs($mIds, 'apiId, mId');
        $aIds = array_keys($apiMenus);
        $Model_Api = new Model_Api();
        $apis = $Model_Api->getDataByAIdFetchPairs($aIds);
        $temp = array();
        $Model_ProjectMenu = new Model_ProjectMenu();
        $projectMenu = $Model_ProjectMenu->getDataByMIdFetchPairsMId($mIds);
        foreach ($apiMenus as $k=>$v){
            $temp[$v['mId']]['api'] = DI()->base->getIndex(DI()->base->getIndex($apis, $k), 'apiname');
            $temp[$v['mId']]['apiName'] = DI()->base->getIndex(DI()->base->getIndex($apis, $k), 'name');
            $temp[$v['mId']]['apiId'] = DI()->base->getIndex(DI()->base->getIndex($apis, $k), 'apiId');
        }

        foreach ($menus as $k=>$v){
            $menus[$k]['api'] = DI()->base->getIndex(DI()->base->getIndex($temp, $k), 'api');
            $menus[$k]['apiName'] = DI()->base->getIndex(DI()->base->getIndex($temp, $k), 'apiName');
            $menus[$k]['apiId'] = DI()->base->getIndex(DI()->base->getIndex($temp, $k), 'apiId');
            $menus[$k]['pId'] = DI()->base->getIndex(DI()->base->getIndex($projectMenu, $k), 'pId');
        }
        return array_values($menus);
    }


    /**
     * 添加接口
     */
    public function addMenu($name, $apiId, $pId){
        $this->checkApiId($apiId);
        $this->checkMenuName($name);
        $Model_Menu = new Model_Menu();
        $rs = $Model_Menu->insertData(array('name'=>$name));
        if(!$rs)
            throw new PhalApi_Exception_BadRequest('新增接口失败');
        $mId = $Model_Menu->lastInsertId();
        $Model_ApiMenu = new Model_ApiMenu();
        $rs = $Model_ApiMenu->insertData(array('mId'=>$mId, 'apiId'=>$apiId));
        if(!$rs)
            throw new PhalApi_Exception_BadRequest('新增菜单失败');

        $Model_ProjectMenu = new Model_ProjectMenu();
        $info = $Model_ProjectMenu->getDataByPIdAndMId($pId, $mId);
        echo 55555;
        if(!$info){
            $rs = $Model_ProjectMenu->insertData(array('pId'=> $pId, 'mId'=> $mId));
            if(!$rs)
                throw new PhalApi_Exception_BadRequest('新增菜单失败');
        }
    }

    /**
     * 检验apiname是否重复
     */
    private function checkMenuName($MenuName, $mId = 0){
        $Model_Menu = new Model_Menu();
        if($mId){
            $this->checkMId($mId);
            $rs = $Model_Menu->getDataBymIdAndName($mId, $MenuName, 'mId');
        }else{
            $rs = $Model_Menu->getDataByName($MenuName);
        }
        if($rs)
            throw new PhalApi_Exception_BadRequest('菜单名称重复');
    }

    /**
     * 检验aId是否存在
     */
    private function checkMId($mId){
        $Model_Menu = new Model_Menu();
        $rs = $Model_Menu->getDataByMId($mId, 'mId');
        if(!$rs)
            throw new PhalApi_Exception_BadRequest('不存在此菜单');
    }

    /**
     * 检查apiId
     */
    private function checkApiId($apiId, $mId = 0){
        $Model_ApiMenu = new Model_ApiMenu();
        if($mId){
            $rs = $Model_ApiMenu->getDataByApiIdAndMId($apiId, $mId);
        }else{
            $rs = $Model_ApiMenu->getDataByApiId($apiId);
        }
        if($rs)
            throw new PhalApi_Exception_BadRequest('接口已被其他菜单占用');
    }

    /**
     * 修改接口
     */
    function updateMenu($mId, $name, $apiId, $pId){
        $this->checkApiId($apiId, $mId);
        $this->checkMenuName($name, $mId);
        $Model_Menu = new Model_Menu();
        $rs = $Model_Menu->updateDataByMId($mId, array('name'=>$name));
        if($rs === false)
            throw new PhalApi_Exception_BadRequest('修改菜单失败');
        $Model_ApiMenu = new Model_ApiMenu();
        $rs = $Model_ApiMenu->updateDataByMId($mId, array('apiId'=>$apiId));
        if($rs === false)
            throw new PhalApi_Exception_BadRequest('修改菜单失败');
        $Model_ProjectMenu = new Model_ProjectMenu();
        $rs = $Model_ProjectMenu->updateDataByMId($mId, array('pId'=> $pId));
        if($rs === false)
            throw new PhalApi_Exception_BadRequest('修改菜单失败');
    }

    /**
     * 修改启用状态
     */
    public function updateMenuType($mId, $type){
        $this->checkMId($mId);
        $Model_Menu = new Model_Menu();
        $rs = $Model_Menu->updateDataByMId($mId, array('type'=>$type));
        if($rs === false)
            throw new PhalApi_Exception_BadRequest('更改状态失败');
    }

    /**
     * 删除接口
     */
    public function deleteMenu($mId){
        $this->checkMId($mId);
        $Model_Menu = new Model_Menu();
        $rs = $Model_Menu->deleteDataByMId($mId);
        if(!$rs)
            throw new PhalApi_Exception_BadRequest('删除菜单失败');
        $Model_ApiMenu = new Model_ApiMenu();
        $rs = $Model_ApiMenu->deleteDataByMId($mId);
        if($rs === false)
            throw new PhalApi_Exception_BadRequest('删除菜单失败');
        $Model_ProjectMenu = new Model_ProjectMenu();
        $rs = $Model_ProjectMenu->deleteDataByMId($mId);
        if($rs === false)
            throw new PhalApi_Exception_BadRequest('删除菜单失败');
    }
}