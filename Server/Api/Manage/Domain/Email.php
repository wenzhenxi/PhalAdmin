<?php

/**
 * 邮件Domain 类
 * @author: 喵了个咪  <wenzhenxi@vip.qq.com> 2015-07-23
 */
class Domain_Email {

    /**
     * 发送重置密码邮件
     */
    public function sendResetPassWordEmail($email, $url, $name, $resetPassWordKey) {


        $PHPMailer = new PHPMailer_Lite(true);
        $rs = $PHPMailer->send($email, "PhalAdmin重置密码", T('Email ResetPassWord{url}{name}{email}{date}', array(
            "url"   => $url . $resetPassWordKey,
            "name"  => $name,
            "email" => $email,
            "date"  => date("Y-m-d H:i:s")
        )), true);
        if (!$rs) {
            throw new PhalApi_Exception_BadRequest(T("Email Send Error"));
        }
    }

    /**
     * 发送用户密码通知邮件
     */
}