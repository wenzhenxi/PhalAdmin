<?php

/**
 * 开发者Domain 类
 * @author: 喵了个咪  <wenzhenxi@vip.qq.com> 2015-07-23
 */
class Domain_Role {

    /**
     * 获取角色简介清单
     */
    public function getSimpleRoleList() {

        $Model_Role = new Model_Role();
        return $Model_Role->getSimpleRoleList();
    }

    /**
     * 添加角色与角色关联
     */
    public function setAdminIdRId($adminId, $rId) {

        //添加角色与角色关联
        $Model_ManageRole = new Model_ManageRole();
        $Model_ManageRole->setAdminIdRId($adminId, $rId);
    }

    /**
     * 更新用户角色绑定
     */
    public function setAdminIdRIdList($adminId, $roleId) {

        //删除所有用户关联
        $Model_ManageRole = new Model_ManageRole();
        $rs               = $Model_ManageRole->delAdminId($adminId);
        //重新建立关联
        foreach ($roleId as $v) {
            $Model_ManageRole->setAdminIdRId($adminId, $v);
        }
    }

    /**
     * 获取角色列表
     */
    public function getRoleList($keyword) {

        //获取角色列表
        $Model_Role          = new Model_Role();
        $Model_ManageRole    = new Model_ManageRole();
        $Model_RolePrivilege = new Model_RolePrivilege();
        $Model_Privilege     = new Model_Privilege();

        $RoleList = $Model_Role->getRoleList($keyword);
        foreach ($RoleList as $k => $v) {
            //获取莫个角色有多少个用户
            $rs                        = $Model_ManageRole->countadminId(DI()->base->getIndex($v, 'rId'));
            $RoleList[$k]['memberNum'] = $rs;
            //通过rId获取权限ID列表
            $rs              = $Model_RolePrivilege->getpriIdByPrivilegeIdlist(DI()->base->getIndex($v, 'rId'));
            $PrivilegeIdlist = array();
            foreach ($rs as $value) {
                $PrivilegeIdlist[] = $value['priId'];
            }
            //通过权限ID列表 拿出列表
            $permission                 = $Model_Privilege->getSimplePrivilegeList($PrivilegeIdlist);
            $RoleList[$k]['permission'] = $permission;
        }
        return $RoleList;
    }

    /**
     * 添加角色
     */
    public function setRole($name, $info) {

        $Model_Role = new Model_Role();
        $rId        = $Model_Role->setRole($name, $info);
        if (!$rId) {
            throw new PhalApi_Exception_BadRequest(T('No setRole'));
        }
        return $rId;
    }

    /**
     * 修改角色
     */
    public function uodateRole($rId, $name, $info) {

        $Model_Role = new Model_Role();
        $Model_Role->uodateRole($rId, $name, $info);
    }

    /**
     * 修改角色
     */
    public function uodateRoleType($rId, $type) {

        $Model_Role = new Model_Role();
        $Model_Role->uodateRoletype($rId, $type);
    }

    /**
     * 验证角色名
     */
    public function validationNmae($name) {

        //通过名称获取ID
        $Model_Role = new Model_Role();
        $rs         = $Model_Role->validationNmae($name);
        if ($rs) {
            throw new PhalApi_Exception_BadRequest(T('Role Repeat'));
        }
    }

    /**
     * 验证角色名
     */
    public function validationNmaeupdate($name, $rId) {

        //通过名称获取ID
        $Model_Role = new Model_Role();
        $rs         = $Model_Role->validationNmaeupdate($name, $rId);
        if ($rs) {
            throw new PhalApi_Exception_BadRequest(T('Role Repeat'));
        }
    }

    /**
     * 删除角色
     */
    public function deleteRole($rId){
        $Model_Role = new Model_Role();
        $rs = $Model_Role->deleteDataByRId($rId);
        if(!$rs)
            throw new PhalApi_Exception_BadRequest(T('delete failure'));
    }
}