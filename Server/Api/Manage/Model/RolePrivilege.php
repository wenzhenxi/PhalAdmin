<?php

/**
 * Api Model 类
 * @author: 喵了个咪  <wenzhenxi@vip.qq.com> 2015-07-23
 */
class Model_RolePrivilege extends PhalApi_Model_NotORM {

    protected function getTableName($id) {

        return 'role_privilege';
    }

    /**
     * 通过rId获取权限ID列表
     */
    public function getpriIdByPrivilegeIdlist($rId) {

        return $this->getORM()->select('priId')->where('rId', $rId)->fetchAll();
    }

    /**
     * 删除原有角色拥有的权限
     */
    public function delrId($rId) {

        return $this->getORM()->where('rId', $rId)->delete();
    }

    /**
     * 添加关联
     */
    public function setrIdOrPrivilegeId($rId, $privilegeId) {

        $data          = array();
        $data['rId']   = $rId;
        $data['priId'] = $privilegeId;

        return $this->getORM()->insert($data);
    }

}
