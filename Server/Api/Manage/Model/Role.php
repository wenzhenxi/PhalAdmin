<?php

/**
 * Api Model 类
 * @author: 喵了个咪  <wenzhenxi@vip.qq.com> 2015-07-23
 */
class Model_Role extends PhalApi_Model_NotORM {

    protected function getTableName($id) {

        return 'role';
    }

    /**
     * 获取角色简介清单
     */
    public function getSimpleRoleList() {

        return $this->getORM()->select('rId,name')->where('type', 1)->fetchAll();
    }

    /**
     * 通过ID获取名称
     */
    public function getRoleIdlistBuRoleName($RoleIdlist) {

        return $this->getORM()->select('rId,name')->where('rId', $RoleIdlist)->where('type', 1)->fetchAll();
    }

    /**
     * 获取角色列表
     */
    public function getRoleList($keyword) {
        $orm = $this->getORM()->select('rId,name,info,type');
        if($keyword){
            $orm->where("name Like ?", "%$keyword%");
            $orm->or("info Like ?", "%$keyword%");
        }
        return $orm->fetchAll();
    }

    /**
     * 新增角色
     */
    public function setRole($name, $info) {

        $data         = array();
        $data['name'] = $name;
        $data['info'] = $info;
        return $this->getORM()->insert($data);
    }

    /**
     * 修改角色
     */
    public function uodateRole($rId, $name, $info) {

        $data         = array();
        $data['name'] = $name;
        $data['info'] = $info;
        return $this->getORM()->where('rId', $rId)->update($data);
    }

    /**
     * 修改角色
     */
    public function uodateRoletype($rId, $type) {

        $data         = array();
        $data['type'] = $type;
        return $this->getORM()->where('rId', $rId)->update($data);
    }

    /**
     * 过滤ID
     */
    public function filterId($RoleIdList) {

        return $this->getORM()->select('rId')->where('rId', $RoleIdList)->where('type', 1)->fetchAll();
    }

    /**
     * 通过名称获取ID
     */
    public function validationNmae($name) {

        return $this->getORM()->select('rId')->where('name', $name)->where('type', 1)->fetchAll();
    }

    /**
     * 通过名称获取ID
     */
    public function validationNmaeupdate($name, $rId) {

        return $this->getORM()->select('rId')->where('name', $name)->where('rId != ?', $rId)->where('type', 1)->fetchAll();
    }

    /**
     * 删除指定role
     */
    public function deleteDataByRId($rId){

        return $this->getORM()->where('rId', $rId)->delete();
    }
}
