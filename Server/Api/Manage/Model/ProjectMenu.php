<?php

/**
 * Api Model 类
 * @author: 喵了个咪  <wenzhenxi@vip.qq.com> 2015-07-23
 */
class Model_ProjectMenu extends PhalApi_Model_NotORM {

    protected function getTableName($id) {

        return 'project_menu';
    }

    /**
     * 通过ID 获取项目名称ID
     */
    public function getmIdBypId($mId) {

        return $this->getORM()->select('pId')->where('mId', $mId)->fetch();
    }

    /**
     * 通过mId获取数据
     */
    public function getDataByMIdFetchPairsMId($mId) {

        return $this->getORM()->where('mId', $mId)->fetchPairs('mId');
    }

    /**
     * 通过pId和apiId获取数据
     */
    public function getDataByPIdAndMId($pId, $mId){

        return $this->getORM()->where('pId', $pId)->where('mId', $mId)->fetch();
    }

    /**
     * 插入数据
     */
    public function insertData(array $data){

        return $this->getORM()->insert($data);
    }

    /**
     * 修改指定mId数据
     */
    public function updateDataByMId($mId, array $data){

        return $this->getORM()->where('mId', $mId)->update($data);
    }

    /**
     * 删除指定mId数据
     */
    public function deleteDataByMId($mId){

        return $this->getORM()->where('mId', $mId)->delete();
    }
}
