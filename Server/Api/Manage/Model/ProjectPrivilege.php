<?php

/**
 * Api Model 类
 * @author: 喵了个咪  <wenzhenxi@vip.qq.com> 2015-07-23
 */
class Model_ProjectPrivilege extends PhalApi_Model_NotORM {

    protected function getTableName($id) {

        return 'project_privilege';
    }

    /**
     * 通过项目ID获取有哪些权限
     */
    public function getProjectIdByPrivilegeIdList($ProjectId) {

        return $this->getORM()->select('priId')->where('pId', $ProjectId)->fetchAll();
    }

    /**
     * 通过权限ID获取项目
     */
    public function getPrivilegeIdByProjectId($ProjectId) {

        return $this->getORM()->select('pId')->where('priId', $ProjectId)->fetch();
    }

    /**
     * 关联权限ID和项目ID
     */
    public function setpriIdOrProjectId($pId, $priId) {

        $data          = array();
        $data['pId']   = $pId;
        $data['priId'] = $priId;
        return $this->getORM()->insert($data);
    }

    /**
     * 删除权限和项目关联
     */
    public function delpriIdOrProjectId($priId) {

        return $this->getORM()->where('priId', $priId)->delete();
    }
}
