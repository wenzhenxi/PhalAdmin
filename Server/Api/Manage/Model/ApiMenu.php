<?php

/**
 * Api Model 类
 * @author: 喵了个咪  <wenzhenxi@vip.qq.com> 2015-07-23
 */
class Model_ApiMenu extends PhalApi_Model_NotORM {

    protected function getTableName($id) {

        return 'api_menu';
    }

    public function getApiIdByMenuId($ApiIdList) {

        return $this->getORM()->select('mId')->where('apiId', $ApiIdList)->fetchAll();
    }

    /**
     * 获取指定apiId的数据
     */
    public function getDataByAIdFetchPairs($aId, $select = '*'){

        return $this->getORM()->select($select)->where('apiId', $aId)->fetchPairs('apiId');
    }

    /**
     * 获取指定mId数据
     */
    public function getDataByMIdFetchPairs($mId, $select = '*'){

        return $this->getORM()->select($select)->where('mId', $mId)->fetchPairs('apiId');
    }

    /**
     * 插入数据
     */
    public function insertData(array $data){

        return $this->getORM()->insert($data);
    }

    /**
     * 更新数据
     */
    public function updateDataByMId($mId, array $data){

        return $this->getORM()->where('mId', $mId)->update($data);
    }

    /**
     * 获取指定apiId数据
     */
    public function getDataByApiId($apiId){

        return $this->getORM()->where('apiId', $apiId)->fetch();
    }

    /**
     * 获取指定apiId数据
     */
    public function getDataByApiIdAndMId($apiId, $mId){

        return $this->getORM()->where('apiId', $apiId)->where('mId != ?', $mId)->fetch();
    }

    /**
     * 删除指定apiId数据
     */
    public function deleteDataByApiId($apiId){

        return $this->getORM()->where('apiId', $apiId)->delete();
    }
    /**
     * 删除指定mId数据
     */
    public function deleteDataByMId($mId){

        return $this->getORM()->where('mId', $mId)->delete();
    }
}
