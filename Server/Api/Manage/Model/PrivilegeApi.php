<?php

/**
 * Api Model 类
 * @author: 喵了个咪  <wenzhenxi@vip.qq.com> 2015-07-23
 */
class Model_PrivilegeApi extends PhalApi_Model_NotORM {

    protected function getTableName($id) {

        return 'privilege_api';
    }

    //权限ID获取有哪些接口
    public function getpriIdByapiId($priId) {

        return $this->getORM()->select('apiId')->where('priId', $priId)->fetchAll();
    }

    /**
     * 删除权限和API关联
     */
    public function delapiListOrpriId($priId) {

        return $this->getORM()->where('priId', $priId)->delete();
    }

    /**
     * 删除权限和API关联
     */
    public function deleteDataByApiId($apiId) {

        return $this->getORM()->where('apiId', $apiId)->delete();
    }
}
