<?php

/**
 * Api Model 类
 * @author: 喵了个咪  <wenzhenxi@vip.qq.com> 2015-07-23
 */
class Model_Api extends PhalApi_Model_NotORM {

    protected function getTableName($id) {

        return 'api';
    }

    /**
     * 通过APIId列表获取接口清单
     */
    public function getapiIdListByapiList($apiIdList) {

        return $this->getORM()->select('apiId,name,apiName')->where('apiId', $apiIdList)->where('type', 1)->fetchAll();
    }

    /**
     * 过滤
     */
    public function filterId($ApiIdList) {

        return $this->getORM()->select('apiId')->where('apiId', $ApiIdList)->where('type', 1)->fetchAll();
    }

    /**
     * 验证用户是否拥有权限
     */
    public function validation($apiname, $ApiIdList) {

        return $this->getORM()->select('apiId')->where('apiId', $ApiIdList)->where('apiname', $apiname)->where('type', 1)->fetchAll();
    }

    /**
     * 获取所有接口
     */
    public function getAllDataFetchPairs($select = '*', $keyword = ''){

        $orm = $this->getORM()->select($select);
        if($keyword){
            $orm->where("name Like ?", "%$keyword%");
            $orm->or("apiname Like ?", "%$keyword%");
        }
        return $orm->fetchPairs('apiId');
    }

    /**
     * 插入数据
     */
    public function insertData(array $data){

        return $this->getORM()->insert($data);
    }

    /**
     * 获取id
     */
    public function lastInsertId(){

        return $this->getORM()->insert_id();
    }

    /**
     * 获取指定接口名数据
     */
    public function getDataByApiName($apiName){

        return $this->getORM()->where('apiname', $apiName)->fetch();
    }

    /**
     * 获取指定接口名数据
     */
    public function getDataByAIdAndApiName($aId, $apiName){

        return $this->getORM()->where('apiname', $apiName)->where('apiId != ?', $aId)->fetch();
    }

    /**
     * 修改指定aId数据
     */
    public function updateDataByAId($aId, array $data){

        return $this->getORM()->where('apiId', $aId)->update($data);
    }

    /**
     * 获取指定api数据
     */
    public function getDataByAId($aId){

        return $this->getORM()->where('apiId', $aId)->fetch();
    }

    /**
     * 获取指定api数据
     */
    public function getDataByAIdFetchPairs($aId){

        return $this->getORM()->where('apiId', $aId)->fetchPairs('apiId');
    }

    /**
     * 删除指定接口
     */
    public function deleteDataByAId($aId){

        return $this->getORM()->where('apiId', $aId)->delete();
    }
}
