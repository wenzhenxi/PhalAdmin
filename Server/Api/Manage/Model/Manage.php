<?php

/**
 * Api Model 类
 * @author: 喵了个咪  <wenzhenxi@vip.qq.com> 2015-07-23
 */
class Model_Manage extends PhalApi_Model_NotORM {

    protected function getTableName($id) {

        return 'manage';
    }

    /**
     * 获取用户ID 用户登录
     */
    public function getDevelopersByName($username, $password) {

        return $this->getORM()->select('uId,type,name,phone')->where('userName', $username)->where('passWord', $password)->fetch();
    }

    /**
     * 通过ID 获取管理员信息
     */
    public function getDevelopersById($uId) {

        return $this->getORM()->select('*')->where('type', 1)->where('uId', $uId)->fetch();
    }

    /**
     * 获取管理员列表信息
     */
    public function getManageList($keyword) {

        $orm = $this->getORM()->select('uId,userName,name,type,phone,email,CreateTimeThe');
        if ($keyword) {
            $orm->where("userName Like ?", "%$keyword%");
            $orm->or("name Like ?", "%$keyword%");
            $orm->or("phone Like ?", "%$keyword%");
            $orm->or("email Like ?", "%$keyword%");
        }
        return $orm->fetchAll();
    }

    /**
     * 通过用户名获取管理员Id
     */
    public function getUserNameById($userName) {

        return $this->getORM()->select('uId')->where('userName', $userName)->fetch();
    }

    /**
     * 通过用户邮箱获取管理员Id
     */
    public function getEmailById($email) {

        return $this->getORM()->select('uId')->where('email', $email)->fetch();
    }

    /**
     * 创建用户
     */
    public function increaseManage($userName, $passWord, $name, $phone, $email) {

        $data = array(
            'userName' => $userName,
            'passWord' => $passWord,
            'name'     => $name,
            'phone'    => $phone,
            'email'    => $email
        );

        return $this->getORM()->insert($data);
    }

    /**
     * 修改用户
     */
    public function updateManage($uId, $name, $phone, $email) {
        //如果传递的ID 是 MD5字符串全部为0 则不更新
        $data = array(
            "name"  => $name,
            "phone" => $phone,
            "email" => $email,
        );

        return $this->getORM()->where('uId', $uId)->update($data);
    }

    /**
     * 修改用户
     */
    public function updateManage2($uId, $name, $phone) {

        $data = array();
        $data['name'] = $name;
        $data['phone'] = $phone;
        return $this->getORM()->where('uId', $uId)->update($data);
    }

    /**
     * 修改用户
     */
    public function updateManageType($uId, $type) {

        $data = array();
        $data['type'] = $type;
        return $this->getORM()->where('uId', $uId)->update($data);
    }

    /**
     * 修改用户密码
     */
    public function setPassWord($newPw, $uId) {

        $data = array();
        $data['passWord'] = $newPw;
        return $this->getORM()->where('uId', $uId)->update($data);
    }

    /**
     * 删除用户
     */
    public function deleteManage($uId) {

        return $this->getORM()->where('uId', $uId)->delete();
    }

    /**
     * 修改用户验证邮箱,除了自己不能喝别人的邮箱相同
     */
    public function updateManageCheckEmail($uId, $email) {

        return $this->getORM()->select('uId')->where("uId != ?", $uId)->where('email', $email)->fetch();
    }

    /**
     * 通过邮箱获取详情
     */
    public function getInfoByEmail($email) {

        return $this->getORM()->where("email", $email)->fetch();
    }

    /**
     * 设置找回密码key
     */
    public function setResetPassWordKey($uId, $resetPassWordKey) {

        return $this->getORM()->where("uId", $uId)->update(array(
            "resetPassWordKey"  => $resetPassWordKey,
            "resetPassWordTime" => time()
        ));
    }

}
