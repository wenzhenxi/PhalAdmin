<?php

/**
 * Api Model 类
 * @author: 喵了个咪  <wenzhenxi@vip.qq.com> 2015-07-23
 */
class Model_ManageRole extends PhalApi_Model_NotORM {

    protected function getTableName($id) {

        return 'manage_role';
    }

    /**
     * 通过用户ID获取角色IDlist
     */
    public function getManagIdListByRoleIdList($uId) {

        return $this->getORM()->select('rId')->where('uId', $uId)->fetchAll();
    }

    /**
     * 添加角色与角色关联
     */
    public function setAdminIdRId($uId, $rId) {

        $data = array();
        $data['uId'] = $uId;
        $data['rId'] = $rId;
        return $this->getORM()->insert($data);
    }

    /**
     * 删除所有admin的关联
     */
    public function delAdminId($uId) {

        return $this->getORM()->where('uId', $uId)->delete();
    }

    /**
     * 获取莫个角色有多少个用户
     */
    public function countadminId($rId) {

        return $this->getORM()->where('rId', $rId)->count();
    }
}
