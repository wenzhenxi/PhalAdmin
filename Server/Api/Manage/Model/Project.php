<?php

/**
 * Api Model 类
 * @author: 喵了个咪  <wenzhenxi@vip.qq.com> 2015-07-23
 */
class Model_Project extends PhalApi_Model_NotORM {

    protected function getTableName($id) {

        return 'project';
    }

    /**
     * 通过项目名称获取项目的ID
     */
    public function getNameById($projectname) {

        return $this->getORM()->select('pId')->where('name', $projectname)->where('type', 1)->fetch();
    }

    /**
     * 通过项目名称获取项目的ID
     */
    public function getIdByNmae($pId) {

        return $this->getORM()->select('pId,name')->where('pId', $pId)->where('type', 1)->fetch();
    }

    /**
     * 根据pId获取数据
     */
    public function getDataByPId($pId, $select = '*'){

        return $this->getORM()->select($select)->where('pId', $pId)->fetch();
    }

    /**
     * 获取所有的项目名称
     */
    public function getProjectList() {

        return $this->getORM()->select('pId,name,type')->fetchAll();
    }

    /**
     * 获取所有的项目IDList
     */
    public function getProjectIdList() {

        return $this->getORM()->select('pId,name')->where('type', 1)->fetchAll();
    }

    /**
     * 插入数据
     */
    public function insertData(array $data){

        return $this->getORM()->insert($data);
    }

    /**
     * 根据名称获取数据
     */
    public function getDataByName($name, $select = '*'){

        return $this->getORM()->select($select)->where('name', $name)->fetch();
    }

    public function getDataByPIdAndName($pId, $name, $select = '*'){

        return $this->getORM()->select($select)->where('name', $name)->where('pId != ?', $pId)->fetch();
    }

    /**
     * 更新数据
     */
    public function updateDataByPId($pId, array $data){

        return $this->getORM()->where('pId', $pId)->update($data);
    }

    /**
     * 删除项目
     */
    public function deleteDataByPId($pId){

        return $this->getORM()->where('pId', $pId)->delete();
    }
}
