<?php

/**
 * Api Model 类
 * @author: 喵了个咪  <wenzhenxi@vip.qq.com> 2015-07-23
 */
class Model_ProjectApi extends PhalApi_Model_NotORM {

    protected function getTableName($id) {

        return 'project_api';
    }

    /**
     * 通过项目的ID 获取有哪些接口
     */
    public function getpIdByapiIdList($projectId) {

        return $this->getORM()->select('apiId')->where('pId', $projectId)->fetchAll();
    }

    /**
     * 通过项目Id 和 用户API Id 验证
     */
    public function getApiIdListOrProId($ApiIdList, $ProId) {

        return $this->getORM()->select('apiId')->where('pId', $ProId)->where('apiId', $ApiIdList)->fetchAll();
    }

    /**
     * 获取指定项目的api总数
     */
    public function getCoutByPId($pId){

        return $this->getORM()->where('pId', $pId)->count();
    }

    /**
     * 获取指定项目数据
     */
    public function getDataByPIdFetchPairsAId($pId, $select="*"){

        return $this->getORM()->select($select)->where('pId', $pId)->fetchPairs('apiId');
    }

    /**
     * 获取指定项目数据
     */
    public function getDataByAIdFetchPairsAId($aId, $select="*"){

        return $this->getORM()->select($select)->where('apiId', $aId)->fetchPairs('apiId');
    }

    /**
     * 插入数据
     */
    public function insertData(array $data){

        return $this->getORM()->insert($data);
    }

    /**
     * 更新指定接口数据
     */
    public function updateDataByAId($aId, array $data){

        return $this->getORM()->where('apiId', $aId)->update($data);
    }

    /**
     * 删除指定接口数据
     */
    public function deleteDataByAId($aId){

        return $this->getORM()->where('apiId', $aId)->delete();
    }
}
