<?php

/**
 * Api Model 类
 * @author: 喵了个咪  <wenzhenxi@vip.qq.com> 2015-07-23
 */
class Model_Menu extends PhalApi_Model_NotORM {

    protected function getTableName($id) {

        return 'menu';
    }

    /**
     * 获取菜单列表
     */
    public function getmIdListByList($mIdList) {

        return $this->getORM()->select('mId,icon,url,name')->where('mId', $mIdList)->where('type', 1)->fetchAll();
    }

    /**
     * 获取指定mId数据
     */
    public function getDataByMId($mId, $select = '*'){

        return $this->getORM()->select($select)->where('mId', $mId)->fetch();
    }

    /**
     * 获取所有数据
     */
    public function getAllDataFetchPairs($select = '*', $keyword = ''){

        $orm =  $this->getORM()->select($select);
        if($keyword){
            $orm->where("name Like ?", "%$keyword%");
        }
        return $orm->fetchPairs('mId');
    }

    /**
     * 获取指定菜单名数据
     */
    public function getDataBymIdAndName($mId, $name){

        return $this->getORM()->where('name', $name)->where('mId != ?', $mId)->fetch();
    }

    /**
     * 获取指定菜单名数据
     */
    public function getDataByName($name){

        return $this->getORM()->where('name', $name)->fetch();
    }

    /**
     * 获取指定接口名数据
     */
    public function getDataByAIdAndApiName($aId, $apiName){

        return $this->getORM()->where('apiname', $apiName)->where('apiId != ?', $aId)->fetch();
    }

    /**
     * 插入数据
     */
    public function insertData(array $data){

        return $this->getORM()->insert($data);
    }

    /**
     * 获取id
     */
    public function lastInsertId(){

        return $this->getORM()->insert_id();
    }

    /**
     * 修改指定aId数据
     */
    public function updateDataByMId($mId, array $data){

        return $this->getORM()->where('mId', $mId)->update($data);
    }

    /**
     * 删除指定菜单
     */
    public function deleteDataByMId($mId){

        return $this->getORM()->where('mId', $mId)->delete();
    }
}
