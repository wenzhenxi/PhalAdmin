<?php

/**
 * Api Model 类
 * @author: 喵了个咪  <wenzhenxi@vip.qq.com> 2015-07-23
 */
class Model_Privilege extends PhalApi_Model_NotORM {

    protected function getTableName($id) {

        return 'privilege';
    }

    /**
     * 通过ID列表获取简介详情列表
     */
    public function getSimplePrivilegeList($PrivilegeIdList) {

        return $this->getORM()->select('priId,name')->where('priId', $PrivilegeIdList)->where('type', 1)->fetchAll();
    }

    /**
     * 权限列表
     */
    public function getPrivilegeList($keyword) {

        $orm = $this->getORM()->select('priId,name,info,type');
        if($keyword){
            $orm->where("name Like ?", "%$keyword%");
            $orm->or("info Like ?", "%$keyword%");
        }
        return $orm->fetchAll();
    }

    /**
     * 添加权限
     */
    public function setPrivilege($name, $info) {

        $data         = array();
        $data['name'] = $name;
        $data['info'] = $info;
        return $this->getORM()->insert($data);
    }

    /**
     * 修改权限
     */
    public function updatePrivilege($priId, $name, $info) {

        $data         = array();
        $data['name'] = $name;
        $data['info'] = $info;
        return $this->getORM()->where('priId', $priId)->update($data);
    }

    /**
     * 修改权限
     */
    public function updatePrivilegeType($priId, $type) {

        $data         = array();
        $data['type'] = $type;
        return $this->getORM()->where('priId', $priId)->update($data);
    }

    /**
     * 过滤权限
     */
    public function filterId($PrivilegeIdList) {

        return $this->getORM()->select('priId')->where('priId', $PrivilegeIdList)->where('type', 1)->fetchAll();
    }

    /**
     * 删除权限
     */
    public function deleteDataByPriId($priId){

        return $this->getORM()->where('priId', $priId)->delete();
    }
}
