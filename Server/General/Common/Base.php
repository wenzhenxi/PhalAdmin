<?php

/**
 * PhalApi_Bse 基础函数
 */
class Common_Base {

    public $time0;

    public function __construct() {

        $this->time0 = $this->getNowTime();
    }

    public function echoBr($s = '') {

        $this->echoN($s . '<br />');
    }

    public function echoN($s = '') {

        echo "\n" . $s;
    }

    /**
     *页面跳转
     */
    public function redirect($url) {

        header("Location: $url");
        exit;
    }

    /**
     * 直接显示内容数组
     */
    public function showarr($a) {

        $this->echoN('<pre>');
        print_r($a);
        $this->echoN('</pre>');
    }

    /**
     * 获得目前时间 ，单位0.1毫秒
     */
    public function getNowTime() {

        return round(microtime(true) * 10000);
    }

    /**
     * 获得getNowTime到运行now的运行时间
     */
    public function now() {

        echo ($this->getNowTime() - $this->time0) / 10;
    }

    /**
     * 时间转换
     */
    public function compressTime($t, $uncompress = 0) {

        //$base = 1325347200; //2012-01-01
        $base = 1420041600; //2015-01-01
        return $t + ($uncompress ? $base : -$base);
    }

    /**
     *  数组转成json
     */
    public function myJsonEncode($obj) {

        return json_encode($obj, JSON_UNESCAPED_SLASHES + JSON_UNESCAPED_UNICODE);
    }

    /**
     * json转成数组
     */
    public function myJsonDecode($str) {

        return json_decode($str, true);
    }

    /**
     * 数值运算 判断
     */
    public function between($a, $min, $max) {

        return $a < $max && $a > $min;
    }

    /**
     * 计算经纬度两点之间的距离
     */
    public function distance($lat0, $lng0, $lat1, $lng1) {      //计算两点距离
        $dx = ($lat1 - $lat0) * 95214.386153799;
        $dy = ($lng1 - $lng0) * 111319.49100589;
        return round(sqrt($dx * $dx + $dy * $dy));
    }

    /**
     * 数组对象取值相关 - 避免出错
     */
    public function getIndex($arr, $key, $default = '') {

        return isset($arr[$key]) ? $arr[$key] : $default;
    }

    /**
     * 根据路径创建目录 文件夹
     */
    public function mkpath($path) {

        $dir  = explode('/', $path);
        $path = '';
        foreach ($dir as $element) {
            $path .= $element . '/';
            //echoBr($path);
            if (!is_dir($path)) {
                if (!mkdir($path)) {
                    echo "something was wrong at : $path";
                    break;
                }
            }
        }
    }

    /**
     * 获取毫秒时间
     */
    function microtime_float() {

        list($usec, $sec) = explode(" ", microtime());
        return ((float) $usec + (float) $sec);
    }

    /**
     * 验证JSON
     */
    public function is_not_json($str, $name = null) {

        json_decode($str);
        if (!(json_last_error() == JSON_ERROR_NONE)) {
            throw new PhalApi_Exception_BadRequest(T('No Josn') . $name);
        }
        return;
    }

    /**
     * 递归对返回结果转移
     */
    public function recursivemyDeEscape($arr) {

        if (is_array($arr)) {
            foreach ($arr as $k => $v) {
                if (is_string($v)) {
                    $arr[$k] = $this->myDeEscape($v);
                }
                if (is_array($v)) {
                    $arr[$k] = $this->recursivemyDeEscape($v);
                }
            }
        }
        return $arr;
    }

    /**
     * '号转换
     */
    function myEnEscape($str) {

        return str_replace(array("'"), array('%27'), $str);
    }

    /**
     * '号反转意
     */
    function myDeEscape($str) {

        return str_replace(array('%27'), array("'"), $str);
    }

    /**
     * 获取字符串两个字符串中的值
     */
    function get_between($input, $start, $end) {

        $substr = substr($input, strlen($start) + strpos($input, $start), (strlen($input) - strpos($input, $end)) * (-1));
        return $substr;
    }

    /**
     * $str 原始中文字符串
     * $encoding 原始字符串的编码，默认GBK
     * $prefix 编码后的前缀，默认"&#"
     * $postfix 编码后的后缀，默认";"
     */
    function unicode_encode($str, $encoding = 'UTF-8', $prefix = '&#', $postfix = ';') {

        $str    = iconv($encoding, 'UCS-2', $str);
        $arrstr = str_split($str, 2);
        $unistr = '';
        for ($i = 0, $len = count($arrstr); $i < $len; $i++) {
            $dec = hexdec(bin2hex($arrstr[$i]));
            $unistr .= $prefix . $dec . $postfix;
        }
        return $unistr;
    }

    /**
     * $str Unicode编码后的字符串
     * $decoding 原始字符串的编码，默认GBK
     * $prefix 编码字符串的前缀，默认"&#"
     * $postfix 编码字符串的后缀，默认";"
     */
    function unicode_decode($unistr, $encoding = 'UTF-8', $prefix = '&#', $postfix = ';') {

        $arruni = explode($prefix, $unistr);
        $unistr = '';
        for ($i = 1, $len = count($arruni); $i < $len; $i++) {
            if (strlen($postfix) > 0) {
                $arruni[$i] = substr($arruni[$i], 0, strlen($arruni[$i]) - strlen($postfix));
            }
            $temp = intval($arruni[$i]);
            $unistr .= ($temp < 256) ? chr(0) . chr($temp) : chr($temp / 256) . chr($temp % 256);
        }
        return iconv('UCS-2', $encoding, $unistr);
    }
}
